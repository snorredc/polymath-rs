//! Additional documentation. (Only exists in documentation.)

use super::*;

/// # How to make a ring
///
/// This is a short tutorial on creating a ring. For simplicity, we will be
/// implementing the direct product of the integers with itself, in other words,
/// the ring consisting of pairs of integers added and multiplied element-wise.
///
/// To create a ring, we first need to consider how the elements will be
/// represented and what additional information, if any, the ring itself needs.
/// The element representation will in this case simply be `(i32, i32)` and the
/// ring will need no extra information.
///
/// Bringing this together, we declare the basic types of the algebraic
/// structure by defining a type for the ring instance (in this case a `struct`
/// with no fields which we will call `Pairs`) and implement the [`Structure`]
/// trait. This trait then requires us to provide the representation type
/// through the associated types `OwnedRepr`, `MutRepr`, and `Repr`. Often,
/// and in this case, these three types will be identical, but especially when
/// working with slices and other unsized types it can be useful to make them
/// different.
///
/// ```
/// use polymath::{CommutRing, ReprEq, Ring, Structure, Element};
///
/// #[derive(Debug, Clone, Copy, PartialEq)]
/// pub struct Pairs;
///
/// impl Structure for Pairs {
///     type OwnedRepr = (i32, i32);
///     type MutRepr = (i32, i32);
///     type Repr = (i32, i32);
/// }
/// ```
///
/// Once the algebraic structure has been declared, we need to implement the
/// traits [`ReprEq`] and [`Ring`] to make it a ring. The former defines
/// equality on elements while the latter defines the ring operations. Here,
/// the elements are given using their raw representations. In practice, the
/// defined function will be called through the safer high-level interface given
/// by [`Element`] and [`ElementMut`] except when more control over performance
/// is needed.
/// The high-level interface will take care of verifying that the user does not
/// mix elements from different structures so we can focus on the well-defined
/// mathematical operations of our ring.
///
/// We will here only define the minimal set of functions necessary. Many more
/// operations are available as provided functions that can be overriden for
/// performance.
///
/// ```
/// # use polymath::{CommutRing, ReprEq, Ring, Structure, Element};
/// #
/// # #[derive(Debug, Clone, Copy, PartialEq)]
/// # pub struct Pairs;
/// #
/// # impl Structure for Pairs {
/// #     type OwnedRepr = (i32, i32);
/// #     type MutRepr = (i32, i32);
/// #     type Repr = (i32, i32);
/// # }
/// #
/// impl ReprEq for Pairs {
///     fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
///         repr == other
///     }
/// }
///
/// impl Ring for Pairs {
///     fn repr_zero(&self) -> Self::OwnedRepr {
///         (0, 0)
///     }
///
///     fn repr_one(&self) -> Self::OwnedRepr {
///         (1, 1)
///     }
///
///     fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
///         (repr.0 + other.0, repr.1 + other.1)
///     }
///
///     fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
///         (repr.0 - other.0, repr.1 - other.1)
///     }
///
///     fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
///         (repr.0 * other.0, repr.1 * other.1)
///     }
/// }
///
/// impl CommutRing for Pairs {}
/// ```
///
/// We here also implement [`CommutRing`] to indicate that our ring is
/// commutative.
///
/// This is the bare minimum that is necessary to be able to make computations.
///
/// ```
/// # use polymath::{CommutRing, ReprEq, Ring, Structure, Element};
/// #
/// # #[derive(Debug, Clone, Copy, PartialEq)]
/// # pub struct Pairs;
/// #
/// # impl Structure for Pairs {
/// #     type OwnedRepr = (i32, i32);
/// #     type MutRepr = (i32, i32);
/// #     type Repr = (i32, i32);
/// # }
/// #
/// # impl ReprEq for Pairs {
/// #     fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
/// #         repr == other
/// #     }
/// # }
/// #
/// # impl Ring for Pairs {
/// #     fn repr_zero(&self) -> Self::OwnedRepr {
/// #         (0, 0)
/// #     }
/// #
/// #     fn repr_one(&self) -> Self::OwnedRepr {
/// #         (1, 1)
/// #     }
/// #
/// #     fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
/// #         (repr.0 + other.0, repr.1 + other.1)
/// #     }
/// #
/// #     fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
/// #         (repr.0 - other.0, repr.1 - other.1)
/// #     }
/// #
/// #     fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
/// #         (repr.0 * other.0, repr.1 * other.1)
/// #     }
/// # }
/// #
/// # impl CommutRing for Pairs {}
/// #
/// let parent = Pairs;
///
/// let a = parent.one();
/// let b = parent.wrap_unchecked((12, 7));
///
/// assert_eq!(b.add(&a).repr(), &(13, 8));
/// ```
pub mod make_a_ring {}

/// # How to use an existing ring
///
/// This is a short tutorial how to use  existing algebraic structures.
///
/// ## Modular arithmetic
///
/// Two of the simplest algebraic structures we have are the integers and
/// finite fields.
///
/// Let us first create an instance of the integers as a ring.
/// This is done like so:
/// ```
/// use polymath::Ints;
/// let ints = Ints::<i32>::new();
/// ```
///
/// Notice that `Ints` is generic. The type parameter of `Ints` determines the
/// representation of values.
/// This is analogous Rust's own integer types `i32`, `i64`, etc.
/// With our parent `ints` we can start defining some integers and performing
/// arithmetic.
/// ```
/// # use polymath::Ints;
/// # let ints = Ints::<i32>::new();
/// use polymath::Element;
///
/// let fifteen = ints.wrap(15);
/// let minus_two = ints.wrap(-2);
///
/// let thirteen = ints.wrap(13);
/// assert_eq!(fifteen.add(minus_two), thirteen);
/// ```
/// All the basic arithmetic operations such as `add` are exposed from the [`Element`] trait.
///
/// Now let us turn to modular arithmetic by changing our parent to a finite field.
/// Like before we first define our parent:
/// ```
/// use polymath::ModP;
/// let f7 = ModP::new_unchecked(7);
/// ```
/// This creates the finite field of 7 elements which we will call `f7`.
/// Notice the naming of [`ModP::new_unchecked`]. In order for `f7` to actually be a field
/// we must use a prime number (here 7) as the modulus. The constructor does not check whether
/// 7 is prime as it would be infeasible to check whether the input is prime for larger numbers.
/// Some structures like [`Ints`] have no “extra information” that needs to be verified so its
/// constructor is just called [`Ints::new`].
///
/// With our new parent we can now define some elements and perform arithmetic just like for
/// the integers:
/// ```
/// # use polymath::ModP;
/// # let f7 = ModP::new_unchecked(7);
/// use polymath::Element;
///
/// let three = f7.wrap(3);
/// let minus_four = f7.wrap(-4);
///
/// assert_eq!(minus_four, f7.wrap(3));
///
/// let six = f7.wrap(6);
/// assert_eq!(three.add(minus_four), six);
/// ```
/// When working with the field of 7 elements we typically think of it as the integers modulo 7
/// in which case -4 and 3 are equal because -4 ≡ 3 (mod 7).
/// We can see that this is also the case here.
/// Armed with the knowledge of how to contstruct rings and perform arithmetic on elements we
/// will delve into the world of structures that build on other structures.
///
/// ## Polynomials
///
/// In this part we will construct a polynomial ring over a finite field and perform
/// some simple calculations with the polynomials we create.
///
/// First we create the polynomial ring.
/// ```
/// use polymath::{Element, ModP, RandomElem, VecPolynomials, VecPolynomialsElement};
///
/// let f13x = VecPolynomials::new(ModP::new_unchecked(13));
/// let f13 = f13x.base_ring();
/// ```
/// Notice that we first create the entire parent of the polynomial ring `f13x` over
/// the finite field and then extract the underlying field `f13` afterwards.
/// This order may seem backwards at first but the reason is that each algebraic structure
/// that “builds” on an existing structure (like polynomials over a ring) owns the instance
/// of its underlying structure so `f13` is actually a `&ModP`, i.e. a *reference* to the field
/// that lives as the underlying ring of `f13x`.
///
/// Now that we have constructed a concrete ring we can start to define some polynomials.
/// The following code creates two polynomials `p` and `q` in slightly different ways and then
/// adds them.
/// ```
/// # use polymath::{Element, ModP, RandomElem, VecPolynomials, VecPolynomialsElement};
/// #
/// # let f13x = VecPolynomials::new(ModP::new_unchecked(13));
/// # let f13 = f13x.base_ring();
/// let p = f13x.from_coeff_reprs(vec![1, 2, 3, 4]);
/// assert_eq!(p.repr(), &[1, 2, 3, 4]);
///
/// let coeffs = vec![1, -2, 3, -4].into_iter().map(|x| f13.wrap(x));
/// let q = f13x.from_coeffs(coeffs);
/// assert_eq!(q.repr(), &[1, 11, 3, 9]);
///
/// let r = p.add(q);
/// assert_eq!(r, f13x.from_coeff_reprs(vec![2, 0, 6]));
/// ```
/// First we create `p` from the raw representation of the coefficients using [`VecPolynomials::from_coeff_reprs`].
/// This function calls `wrap_unchecked` so we have to be cautius about the input that we give it
/// and make sure that they are in the range that `ModP` expects.
/// A more careful way to create a polynomial is to use [`ModP::wrap`] on each value which makes sure
/// that our values are in the expected range.
/// We can then use [`VecPolynomials::from_coeffs`] to create the polynomial from the elements.
/// This is also the way that you will most likely be working with polynomials in every-day code as
/// you will be using the safer high-level interface through elements rather than raw representations
/// most of the time.
pub mod use_a_ring {}

/// # How to make a function that takes and returns elements
///
/// In this short tutorial, we will make a function that takes two elements `a`
/// and `b` from an arbitary ring and returns `a + b + b`.
///
/// The most straight-forward way to do this is by simply taking the inputs as
/// concrete values of type [`ElemRef`] and let lifetime elision simplify the
/// types. We want `ElemRef`s because every element type can be converted to an
/// `ElemRef` by just borrowing. Doing this we get the following:
///
/// ```
/// use polymath::{BorrowElem, Elem, ElemRef, Element, Ring};
///
/// fn add_twice<'p, T>(a: ElemRef<'_, 'p, T>, b: ElemRef<T>) -> Elem<'p, T>
/// where
///     T: Ring,
/// {
///     a.add(&b).add(&b)
/// }
/// ```
///
/// We need to explicitly name the parent lifetime of the first element because
/// that is the lifetime that the returned element uses. Fortunately, Rust's
/// automatic insertion of lifetime bounds means that we don't have to spell out
/// `T: 'p`. Furthermore, Rust's lifetime elision rules mean we don't have to
/// specify the lifetimes of the second operand. On the other hand, we have to
/// specify two lifetimes for the first operand but at least the representation
/// lifetime can be explicitly elided.
///
/// Unfortunately, taking concrete `ElemRef`s proves to complicate calling the
/// function, as we now need to write `add_twice(a.as_ref(), b.as_ref())`
/// instead of `add_twice(&a, &b)`. To remedy this, at the cost of a more
/// verbose function declaration, we can use the [`BorrowElem`] trait. The
/// declaration becomes:
///
/// ```
/// use polymath::{BorrowElem, Elem, Element, Ring};
///
/// fn add_twice<'p, 'q, T, A, B>(a: A, b: B) -> Elem<'p, T>
/// where
///     T: Ring + 'q,
///     A: BorrowElem<'p, T>,
///     B: BorrowElem<'q, T>,
/// {
///     a.add(&b).add(&b)
/// }
/// ```
///
/// This quickly proves to be more complicated than the first version! First,
/// since we want the two arguments to potentially be of different types, we add
/// two new type parameters `A` and `B`. These have to be element-like, so we
/// add the `BorrowElem` bound. This bound, however, has a lifetime, and Rust
/// unfortunately doesn't support lifetime elision in trait bounds at all, so
/// we have to introduce a second lifetime parameter to the function. Also
/// unfortunately, the automatically introduced `'p` bound for `T` only works
/// because the bound can be inferred directly from the argument and return
/// types, so we need to explicitly add a `T: 'q` bound.
///
/// In any case, this version *does* support calling the function directly with
/// anything element-like as `add_twice(&a, &b)`.
///
/// If you need to reason about the lifetime of the representation of an
/// element, you can replace the `BorrowElem` bound with one using [`Element`].
///
/// ## Mutable version
///
/// To make an argument mutable, the same basic approach still works. We must,
/// however, change the bound of the mutable argument type to use
/// [`BorrowElemMut`] instead and we must mark that argument as mutable with
/// `mut`. Since the new version no longer returns anything, the implicit
/// `T: 'p` bound is gone, so we need to add that ourselves. The result is:
///
/// ```
/// use polymath::{BorrowElem, BorrowElemMut, Elem, ElementMut, Ring};
///
/// fn add_twice_mut<'p, 'q, T, A, B>(mut a: A, b: B)
/// where
///     T: Ring + 'p + 'q,
///     A: BorrowElemMut<'p, T>,
///     B: BorrowElem<'q, T>,
/// {
///     a.add_mut(&b);
///     a.add_mut(&b);
/// }
/// ```
///
/// This version can then be called as `add_twice_mut(&mut a, &b)`.
///
/// If you need to reason about the lifetime of the representation of an
/// element, you can replace the `BorrowElemMut` bound with one using
/// [`ElementMut`].
pub mod make_a_function {}
