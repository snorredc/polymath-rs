//! Dense univariate polynomial rings.
//!
//! The central type is [`VecPolynomials`].
//!
//! The extension trait [`VecPolynomialsElement`] provides additional functions
//! for elements over `VecPolynomials`.

use std::borrow::{Borrow, BorrowMut};
use std::cmp::Ordering::{Equal, Greater, Less};
use std::fmt::{self, Debug};
use std::iter::FusedIterator;

use super::{
    BorrowElem, CommutRing, CommutScale, DivisionRing, Elem, ElemRef, Element, FromBorrowed,
    QuoRem, ReprDebug, ReprEq, Ring, Scale, Structure,
};

/// Dense univariate polynomial ring over `T`.
///
/// The polynomials are represented as dense lists of coefficients stored in a
/// [`Vec`].
///
/// The coefficients are stored according to degree, i.e., with the coefficient
/// for the lowest-degree monomial first.
#[derive(Clone, PartialEq)]
pub struct VecPolynomials<T> {
    base_ring: T,
}

impl<T> VecPolynomials<T> {
    /// Returns a polynomial ring with coefficient from `base_ring`.
    #[inline]
    pub const fn new(base_ring: T) -> Self {
        VecPolynomials { base_ring }
    }

    /// Returns the base ring of `self`.
    #[inline]
    pub const fn base_ring(&self) -> &T {
        &self.base_ring
    }

    /// Consumes `self` and returns the base ring.
    #[inline]
    pub fn into_base_ring(self) -> T {
        self.base_ring
    }
}

impl<T> VecPolynomials<T>
where
    T: Ring,
{
    /// Creates a polynomial from raw representations.
    ///
    /// This function does not verify that the representations are valid. It
    /// does, however, canonicalize the polynomial by removing trailing zeroes.
    #[inline]
    pub fn from_coeff_reprs<I>(&self, reprs: I) -> Elem<Self>
    where
        I: IntoIterator<Item = T::OwnedRepr>,
    {
        let mut result = reprs.into_iter().collect();
        self.repr_truncate(&mut result);
        self.wrap_unchecked(result)
    }

    /// Creates a polynomial from raw coefficients.
    #[inline]
    pub fn from_coeffs<'a, I>(&self, coeffs: I) -> Elem<Self>
    where
        I: IntoIterator<Item = Elem<'a, T>>,
        T: 'a,
    {
        self.from_coeff_reprs(coeffs.into_iter().map(|elem| {
            let (repr, parent) = elem.into_pair();
            assert!(*parent == self.base_ring);
            repr
        }))
    }

    /// Canonicalizes `repr` by removing trailing zeroes.
    #[inline]
    pub fn repr_truncate(&self, repr: &mut Vec<T::OwnedRepr>) {
        let mut n = repr.len();
        while n > 0 && self.base_ring.repr_is_zero(repr[n - 1].borrow()) {
            n -= 1;
        }
        repr.truncate(n);
    }
}

/// Extension trait for elements over [`VecPolynomials`].
///
/// This trait is automatically implemented for [`Elem`], [`ElemRef`] and
/// [`ElemMut`] over when the type is a `VecPolynomials`.
///
/// [`ElemMut`]: super::ElemMut
pub trait VecPolynomialsElement<'a, 'p, T>: Element<'a, 'p, VecPolynomials<T>>
where
    T: Structure + 'p,
    T::OwnedRepr: 'a,
{
    /// Returns the number of coefficients in `self`.
    ///
    /// **Note** that this corresponds to one more than the degree of the polynomial and is 0 for
    /// the zero-polynomial.
    ///
    /// In the future there may exist an additional `degree` method.
    /// This requires a decision on how to represent the degree of the zero-polynomial.
    #[inline]
    fn len(self) -> usize {
        self.repr().len()
    }

    /// Returns the coefficient of `self` corresponding to the monomial `X^i`.
    #[inline]
    fn coeff(self, i: usize) -> ElemRef<'a, 'p, T> {
        let (repr, parent) = self.pair();
        parent.base_ring.wrap_ref_unchecked(repr[i].borrow())
    }

    /// Returns the coefficients of `self` as an iterator.
    #[inline]
    fn coeffs(self) -> Coeffs<'a, 'p, T> {
        let poly = self.as_ref();
        Coeffs {
            poly,
            i: 0,
            j: poly.repr().len(),
        }
    }

    /// Splits `self` into the terms of degree less than `i` and the terms of
    /// degree greater than or equal to `i`.
    fn split_at(
        self,
        i: usize,
    ) -> (
        ElemRef<'a, 'p, VecPolynomials<T>>,
        ElemRef<'a, 'p, VecPolynomials<T>>,
    )
    where
        T: Ring,
    {
        let (repr, parent) = self.pair();
        let (lo, hi) = repr.split_at(i);

        let mut n = lo.len();
        while n > 0 && parent.base_ring.repr_is_zero(lo[n - 1].borrow()) {
            n -= 1;
        }
        let lo = &lo[..n];

        (parent.wrap_ref_unchecked(lo), parent.wrap_ref_unchecked(hi))
    }

    /// Returns the formal derivative of `self`.
    fn derivative(self) -> Elem<'p, VecPolynomials<T>>
    where
        T: Ring,
    {
        let (repr, parent) = self.pair();
        let mut repr = repr[1..].to_vec();

        for (i, coeff) in repr.iter_mut().enumerate() {
            parent.base_ring.repr_mul_nat_mut(coeff.borrow_mut(), i + 1);
        }

        // TODO: Specialize on integral domain.
        parent.repr_truncate(&mut repr);

        parent.wrap_unchecked(repr)
    }

    /// Evaluates `self` at the point `x`.
    ///
    /// Note that the point `x` can be of any type that can be [scale]d by the coefficients of `self`.
    ///
    /// [scale]: Scale
    fn eval<'q, U, E>(self, x: E) -> Elem<'q, U>
    where
        U: CommutScale<T> + Ring + 'q,
        E: BorrowElem<'q, U>,
    {
        let (x, p) = x.borrow_elem().pair();
        let (coeffs, parent) = self.pair();

        let mut r = p.repr_zero();
        for c in coeffs.iter().rev() {
            p.repr_mul_mut(r.borrow_mut(), &x);
            p.repr_add_scalar_mut(parent.base_ring(), r.borrow_mut(), c.borrow());
        }
        p.wrap_unchecked(r)
    }
}

impl<'a, 'p, T, E> VecPolynomialsElement<'a, 'p, T> for E
where
    T: Structure + 'p,
    T::OwnedRepr: 'a,
    E: Element<'a, 'p, VecPolynomials<T>>,
{
}

/// Coefficient iterator.
///
/// Created by the [`coeffs`] method on [`VecPolynomialsElement`].
///
/// [`coeffs`]: VecPolynomialsElement::coeffs
#[derive(Clone)]
pub struct Coeffs<'a, 'p, T>
where
    T: Structure,
    T::OwnedRepr: 'a,
{
    poly: ElemRef<'a, 'p, VecPolynomials<T>>,
    i: usize,
    j: usize,
}

impl<'a, 'p, T> Iterator for Coeffs<'a, 'p, T>
where
    T: Structure,
    T::OwnedRepr: 'a,
    T::Repr: 'a,
{
    type Item = ElemRef<'a, 'p, T>;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if self.i < self.j {
            let i = self.i;
            self.i += 1;
            Some(self.poly.coeff(i))
        } else {
            None
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.j - self.i, Some(self.j - self.i))
    }
}

impl<'a, 'p, T> DoubleEndedIterator for Coeffs<'a, 'p, T>
where
    T: Structure,
    T::OwnedRepr: 'a,
    T::Repr: 'a,
{
    #[inline]
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.i < self.j {
            self.j -= 1;
            Some(self.poly.coeff(self.j))
        } else {
            None
        }
    }
}

impl<'a, 'p, T> ExactSizeIterator for Coeffs<'a, 'p, T>
where
    T: Structure,
    T::OwnedRepr: 'a,
    T::Repr: 'a,
{
}

impl<'a, 'p, T> FusedIterator for Coeffs<'a, 'p, T>
where
    T: Structure,
    T::OwnedRepr: 'a,
    T::Repr: 'a,
{
}

impl<T> Structure for VecPolynomials<T>
where
    T: Structure,
{
    type OwnedRepr = Vec<T::OwnedRepr>;
    type MutRepr = Vec<T::OwnedRepr>;
    type Repr = [T::OwnedRepr];
}

impl<T> ReprEq for VecPolynomials<T>
where
    T: ReprEq,
{
    fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
        repr.len() == other.len()
            && repr
                .iter()
                .zip(other)
                .all(|(a, b)| self.base_ring.repr_eq(a.borrow(), b.borrow()))
    }
}

impl<T> Debug for VecPolynomials<T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_tuple("VecPolynomials")
            .field(&self.base_ring)
            .finish()
    }
}

impl<T> ReprDebug for VecPolynomials<T>
where
    T: ReprDebug,
{
    fn fmt(&self, repr: &Self::Repr, f: &mut fmt::Formatter) -> fmt::Result {
        let mut list = f.debug_list();
        for coeff in self.wrap_ref_unchecked(repr).coeffs() {
            list.entry(&coeff);
        }
        list.finish()
    }
}

impl<T> Scale<T> for VecPolynomials<T>
where
    T: Ring,
{
    fn repr_scale(&self, scalar_ring: &T, repr: &Self::Repr, scalar: &T::Repr) -> Self::OwnedRepr {
        assert!(self.base_ring == *scalar_ring);
        repr.iter()
            .map(|coeff| self.base_ring.repr_mul(scalar, coeff.borrow()))
            .collect()
    }
}

impl<T> CommutScale<T> for VecPolynomials<T> where T: CommutRing {}

impl<T> Ring for VecPolynomials<T>
where
    T: Ring,
{
    fn repr_zero(&self) -> Self::OwnedRepr {
        Vec::new()
    }

    fn repr_one(&self) -> Self::OwnedRepr {
        if self.base_ring.is_trivial() {
            Vec::new()
        } else {
            vec![self.base_ring.repr_one()]
        }
    }

    #[inline]
    fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = Self::OwnedRepr::from_borrowed(repr);
        self.repr_add_mut(result.borrow_mut(), other);
        result
    }

    #[inline]
    fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = Self::OwnedRepr::from_borrowed(repr);
        self.repr_sub_mut(result.borrow_mut(), other);
        result
    }

    fn repr_add_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        for (a, b) in repr.iter_mut().zip(other) {
            self.base_ring.repr_add_mut(a.borrow_mut(), b.borrow());
        }

        match repr.len().cmp(&other.len()) {
            Equal => {
                self.repr_truncate(repr);
            }
            Less => {
                repr.extend(other[repr.len()..].iter().cloned());
            }
            Greater => {}
        }
    }

    fn repr_sub_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        for (a, b) in repr.iter_mut().zip(other) {
            self.base_ring.repr_sub_mut(a.borrow_mut(), b.borrow());
        }

        match repr.len().cmp(&other.len()) {
            Equal => {
                self.repr_truncate(repr);
            }
            Less => {
                repr.extend(
                    other[repr.len()..]
                        .iter()
                        .map(|repr| self.base_ring.repr_neg(repr.borrow())),
                );
            }
            Greater => {}
        }
    }

    fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        if repr.is_empty() || other.is_empty() {
            return Vec::new();
        }

        let len = repr.len() + other.len() - 1;
        let mut res = vec![self.base_ring.repr_zero(); len];

        for (i, a) in repr.iter().enumerate() {
            for (j, b) in other.iter().enumerate() {
                self.base_ring
                    .repr_add_mul_mut(res[i + j].borrow_mut(), a.borrow(), b.borrow());
            }
        }

        // TODO: Only necessary if the base ring isn't an integral domain.
        self.repr_truncate(&mut res);

        res
    }

    #[inline]
    fn repr_is_zero(&self, repr: &Self::Repr) -> bool {
        repr.is_empty()
    }

    #[inline]
    fn repr_is_one(&self, repr: &Self::Repr) -> bool {
        if self.base_ring.is_trivial() {
            true
        } else {
            repr.len() == 1 && self.base_ring.repr_is_one(repr[0].borrow())
        }
    }

    fn repr_neg(&self, repr: &Self::Repr) -> Self::OwnedRepr {
        repr.iter()
            .map(|a| self.base_ring.repr_neg(a.borrow()))
            .collect()
    }

    fn repr_neg_mut(&self, repr: &mut Self::MutRepr) {
        for value in repr {
            self.base_ring.repr_neg_mut(value.borrow_mut());
        }
    }

    #[inline]
    fn is_trivial(&self) -> bool {
        self.base_ring.is_trivial()
    }
}

impl<T> CommutRing for VecPolynomials<T> where T: CommutRing {}

impl<T> QuoRem for VecPolynomials<T>
where
    T: DivisionRing + CommutRing,
{
    #[inline]
    fn repr_quo_rem(
        &self,
        repr: &Self::Repr,
        other: &Self::Repr,
    ) -> (Self::OwnedRepr, Self::OwnedRepr) {
        let mut rem = Self::OwnedRepr::from_borrowed(repr);
        let quo = self.repr_quo_rem_mut(rem.borrow_mut(), other);
        (quo, rem)
    }

    fn repr_quo_rem_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) -> Self::OwnedRepr {
        let (head, rest) = other.split_last().expect("division by zero");
        let head_inv = self.base_ring.repr_inv(head.borrow());

        let diff = repr.len().saturating_sub(rest.len());
        if diff == 0 {
            return self.repr_zero();
        }

        for i in (0..diff).rev() {
            let coeff = self
                .base_ring
                .repr_mul(repr[i + rest.len()].borrow(), head_inv.borrow());
            for j in 0..rest.len() {
                let t = self.base_ring.repr_mul(rest[j].borrow(), coeff.borrow());
                repr[i + j] = self.base_ring.repr_sub(repr[i + j].borrow(), t.borrow());
            }
            repr[i + rest.len()] = coeff;
        }

        let quo = repr.split_off(rest.len());

        self.repr_truncate(repr);

        quo
    }
}

#[test]
fn test_poly_quo_rem() {
    use super::ModP;

    let parent = VecPolynomials::new(ModP::new_unchecked(17));
    let a = parent.from_coeff_reprs([1, 2, 1, 3, 4].iter().cloned());
    let b = parent.from_coeff_reprs([7, 5, 11, 3].iter().cloned());

    let (q, r) = a.quo_rem(&b);

    assert_eq!(q.mul(&b).add(&r), &a);
}
