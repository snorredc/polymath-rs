//! Algebraic structures.
//!
//! This module contains the basic traits needed to define an algebraic
//! structure.
//!
//! The central trait is [`Structure`].

use std::borrow::{Borrow, BorrowMut};
use std::fmt;

use super::{Elem, ElemMut, ElemRef};

/// A generalization of `Clone` for borrowed data.
///
/// Unlike with [`ToOwned`], a type can implement `FromBorrowed` for multiple
/// different types.
pub trait FromBorrowed<T: ?Sized>: Borrow<T> + Sized {
    /// Creates owned data from borrowed data usually by cloning.
    fn from_borrowed(source: &T) -> Self;
}

impl<T> FromBorrowed<T> for T
where
    T: Clone,
{
    fn from_borrowed(source: &T) -> T {
        source.clone()
    }
}

impl<T> FromBorrowed<[T]> for Vec<T>
where
    T: Clone,
{
    fn from_borrowed(source: &[T]) -> Self {
        source.to_vec()
    }
}

impl<T> FromBorrowed<[T]> for Box<[T]>
where
    T: Clone,
{
    fn from_borrowed(source: &[T]) -> Self {
        source.to_vec().into_boxed_slice()
    }
}

/// A generalization of `Clone` for borrowed data.
///
/// You should implement [`FromBorrowed`] and use the blanket implementation
/// instead of implementing this trait directly.
pub trait CloneBorrowed<T> {
    /// Creates owned data from borrowed data usually by cloning.
    fn clone_borrowed(&self) -> T;
}

impl<T: ?Sized, U> CloneBorrowed<U> for T
where
    U: FromBorrowed<T>,
{
    fn clone_borrowed(&self) -> U {
        U::from_borrowed(self)
    }
}

/// A trait for assigning values.
pub trait Assign<T> {
    /// Assigns `source` to `self`.
    fn assign(&mut self, source: T);
}

impl<T> Assign<T> for T {
    fn assign(&mut self, source: T) {
        *self = source;
    }
}

impl<T> Assign<Box<[T]>> for [T] {
    fn assign(&mut self, source: Box<[T]>) {
        assert_eq!(self.len(), source.len());
        for (a, b) in self.iter_mut().zip(source.into_vec()) {
            *a = b;
        }
    }
}

/// An algebraic structure.
///
/// An instance of a type that implements `Structure` represents a single
/// algebraic structure. The elements of that structure are then represented
/// using the `OwnedRepr`, `MutRepr` and `Repr` associated types.
///
/// In practice, rather than using the low-level representation types, you will
/// usually interact with elements of the algebraic structure through the
/// [`Elem`], [`ElemRef`] and [`ElemMut`] types.
// TODO: Is `Sized` bound useful/meaningful/necessary?
pub trait Structure: PartialEq + Sized {
    /// The owned representation of an element.
    type OwnedRepr: FromBorrowed<Self::Repr> + BorrowMut<Self::MutRepr> + Clone;

    /// The mutably borrowed representation of an element.
    type MutRepr: Borrow<Self::Repr> + Assign<Self::OwnedRepr> + ?Sized;

    /// The borrowed representation of an element.
    type Repr: ?Sized;

    /// Returns an [`Elem`] from an owned representation without checking
    /// whether that representation is valid.
    #[inline]
    fn wrap_unchecked(&self, repr: Self::OwnedRepr) -> Elem<Self> {
        Elem::new_unchecked(repr, self)
    }

    /// Returns an [`Elem`] from a borrowed representation without checking
    /// whether that representation is valid.
    #[inline]
    fn wrap_ref_unchecked<'a>(&self, repr: &'a Self::Repr) -> ElemRef<'a, '_, Self> {
        ElemRef::new_unchecked(repr, self)
    }

    /// Returns an [`Elem`] from a mutably borrowed representation without
    /// checking whether that representation is valid.
    #[inline]
    fn wrap_mut_unchecked<'a>(&self, repr: &'a mut Self::MutRepr) -> ElemMut<'a, '_, Self> {
        ElemMut::new_unchecked(repr, self)
    }
}

/// An algebraic structure with element equality defined.
///
/// Implementing this trait makes [`Elem`], [`ElemRef`] and [`ElemMut`]
/// implement [`PartialEq`].
pub trait ReprEq: Structure {
    /// Returns true if `repr` and `other` represent the same element.
    fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool;
}

/// An algebraic structure where the elements can be debug-formatted.
///
/// Implementing this trait makes [`Elem`], [`ElemRef`] and [`ElemMut`]
/// implement [`Debug`].
///
/// [`Debug`]: std::fmt::Debug
pub trait ReprDebug: Structure {
    /// Formats the element represented by `repr` using the given formatter.
    fn fmt(&self, repr: &Self::Repr, f: &mut fmt::Formatter) -> fmt::Result;
}
