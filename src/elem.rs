//! Algebraic structure elements.
//!
//! This module contains the high-level interface for working with elements of
//! algebraic structures.
//!
//! The central concrete types are [`Elem`], [`ElemRef`] and [`ElemMut`], which
//! represents owned, borrowed and mutably borrowed elements, respectively.
//! These mirror Rust's `T`, `&T`, and `&mut T` with the addition of
//! a reference to the [`Structure`] of the element.
//!
//! Most functions and operations are available through the [`Element`] and
//! [`ElementMut`] traits. This traits are automatically implemented for
//! immutable and mutable references as appropriate whenever the underlying type
//! implements [`BorrowElem`] and/or [`BorrowElemMut`].
//!
//!
//! # References
//!
//! A side-effect of [`ElemRef`] is that an [`ElemRef`] can act as
//! an element over any borrowed [`Repr`].
//! Hence [`ElemRef`] can be seen not only as a reference to an existing [`Elem`] but
//! also as a reification of a borrowed [`Repr`] making it possible to regard the same
//! stored representation in different ways.
//! An example of this can be seen in [`split_at`].
//!
//! [`Repr`]: Structure::Repr
//! [`split_at`]: super::VecPolynomialsElement::split_at

use std::borrow::{Borrow, BorrowMut};
use std::fmt::{self, Debug};

use super::{
    CloneBorrowed, DivisionRing, Natural, QuoRem, ReprDebug, ReprEq, ReprPartialDiv, Ring, Scale,
    Structure,
};

////////////////////////////////////////////////////////////////////////
// BorrowElem
////////////////////////////////////////////////////////////////////////

/// A trait for borrowing elements.
pub trait BorrowElem<'p, T>
where
    T: Structure,
{
    /// Borrows `self` as an element.
    fn borrow_elem(&self) -> ElemRef<'_, 'p, T>;
}

/// A trait for mutably borrowing elements.
pub trait BorrowElemMut<'p, T>: BorrowElem<'p, T>
where
    T: Structure,
{
    /// Borrows `self` mutably as an element.
    fn borrow_elem_mut(&mut self) -> ElemMut<'_, 'p, T>;
}

impl<'p, E, T> BorrowElem<'p, T> for &E
where
    E: BorrowElem<'p, T>,
    T: Structure,
{
    #[inline]
    fn borrow_elem(&self) -> ElemRef<'_, 'p, T> {
        (**self).borrow_elem()
    }
}

impl<'p, E, T> BorrowElem<'p, T> for &mut E
where
    E: BorrowElem<'p, T>,
    T: Structure,
{
    #[inline]
    fn borrow_elem(&self) -> ElemRef<'_, 'p, T> {
        (**self).borrow_elem()
    }
}

impl<'p, E, T> BorrowElemMut<'p, T> for &mut E
where
    E: BorrowElemMut<'p, T>,
    T: Structure,
{
    #[inline]
    fn borrow_elem_mut(&mut self) -> ElemMut<'_, 'p, T> {
        (**self).borrow_elem_mut()
    }
}

impl<'p, T> BorrowElem<'p, T> for ElemRef<'_, 'p, T>
where
    T: Structure,
{
    #[inline]
    fn borrow_elem(&self) -> ElemRef<'_, 'p, T> {
        *self
    }
}

impl<'p, T> BorrowElem<'p, T> for ElemMut<'_, 'p, T>
where
    T: Structure,
{
    #[inline]
    fn borrow_elem(&self) -> ElemRef<'_, 'p, T> {
        ElemRef::new_unchecked((*self.repr).borrow(), self.parent)
    }
}

impl<'p, T> BorrowElem<'p, T> for Elem<'p, T>
where
    T: Structure,
{
    #[inline]
    fn borrow_elem(&self) -> ElemRef<'_, 'p, T> {
        ElemRef::new_unchecked(self.repr.borrow(), self.parent)
    }
}

impl<'p, T> BorrowElemMut<'p, T> for ElemMut<'_, 'p, T>
where
    T: Structure,
{
    #[inline]
    fn borrow_elem_mut(&mut self) -> ElemMut<'_, 'p, T> {
        ElemMut::new_unchecked(self.repr, self.parent)
    }
}

impl<'p, T> BorrowElemMut<'p, T> for Elem<'p, T>
where
    T: Structure,
{
    #[inline]
    fn borrow_elem_mut(&mut self) -> ElemMut<'_, 'p, T> {
        ElemMut::new_unchecked(self.repr.borrow_mut(), self.parent)
    }
}

////////////////////////////////////////////////////////////////////////
// FromRepr
////////////////////////////////////////////////////////////////////////

trait FromRepr<'p, T, A>: Sized
where
    T: Structure,
{
    fn from_repr(repr: A, parent: &'p T) -> Self;
}

impl<'p, T> FromRepr<'p, T, ()> for ()
where
    T: Structure,
{
    #[inline]
    fn from_repr(_repr: (), _parent: &'p T) -> Self {}
}

impl<'p, T> FromRepr<'p, T, T::OwnedRepr> for Elem<'p, T>
where
    T: Structure,
{
    #[inline]
    fn from_repr(repr: T::OwnedRepr, parent: &'p T) -> Self {
        parent.wrap_unchecked(repr)
    }
}

impl<'p, T> FromRepr<'p, T, bool> for bool
where
    T: Structure,
{
    #[inline]
    fn from_repr(repr: bool, _parent: &'p T) -> Self {
        repr
    }
}

impl<'p, T, A, X> FromRepr<'p, T, Option<X>> for Option<A>
where
    T: Structure,
    A: FromRepr<'p, T, X>,
{
    #[inline]
    fn from_repr(repr: Option<X>, parent: &'p T) -> Self {
        repr.map(|repr| A::from_repr(repr, parent))
    }
}

impl<'p, T, A, B, X, Y> FromRepr<'p, T, Result<X, Y>> for Result<A, B>
where
    T: Structure,
    A: FromRepr<'p, T, X>,
    B: FromRepr<'p, T, Y>,
{
    #[inline]
    fn from_repr(repr: Result<X, Y>, parent: &'p T) -> Self {
        match repr {
            Ok(repr) => Ok(A::from_repr(repr, parent)),
            Err(repr) => Err(B::from_repr(repr, parent)),
        }
    }
}

impl<'p, T, A, B, X, Y> FromRepr<'p, T, (X, Y)> for (A, B)
where
    T: Structure,
    A: FromRepr<'p, T, X>,
    B: FromRepr<'p, T, Y>,
{
    #[inline]
    fn from_repr(repr: (X, Y), parent: &'p T) -> Self {
        (A::from_repr(repr.0, parent), B::from_repr(repr.1, parent))
    }
}

impl<'p, T, A, B, C, X, Y, Z> FromRepr<'p, T, (X, Y, Z)> for (A, B, C)
where
    T: Structure,
    A: FromRepr<'p, T, X>,
    B: FromRepr<'p, T, Y>,
    C: FromRepr<'p, T, Z>,
{
    #[inline]
    fn from_repr(repr: (X, Y, Z), parent: &'p T) -> Self {
        (
            A::from_repr(repr.0, parent),
            B::from_repr(repr.1, parent),
            C::from_repr(repr.2, parent),
        )
    }
}

#[track_caller]
#[inline]
fn unop<'p, T, R, U, A, F>(a: A, f: F) -> R
where
    T: Structure + 'p,
    R: FromRepr<'p, T, U>,
    A: BorrowElem<'p, T>,
    F: FnOnce(&T, &T::Repr) -> U,
{
    let (repr, parent) = a.pair();

    let repr = f(parent, repr);
    R::from_repr(repr, parent)
}

#[track_caller]
#[inline]
fn unop_mut<'p, T, R, U, A, F>(mut a: A, f: F) -> R
where
    T: Structure + 'p,
    R: FromRepr<'p, T, U>,
    A: BorrowElemMut<'p, T>,
    F: FnOnce(&T, &mut T::MutRepr) -> U,
{
    let (repr, parent) = a.pair_mut();

    let repr = f(parent, repr);
    R::from_repr(repr, parent)
}

#[track_caller]
#[inline]
fn binop<'p, 'q, T, R, U, A, B, F>(a: A, b: B, f: F) -> R
where
    T: Structure + 'p + 'q,
    R: FromRepr<'p, T, U>,
    A: BorrowElem<'p, T>,
    B: BorrowElem<'q, T>,
    F: FnOnce(&T, &T::Repr, &T::Repr) -> U,
{
    let (a_repr, a_parent) = a.pair();
    let (b_repr, b_parent) = b.pair();

    assert!(a_parent == b_parent);

    let repr = f(a_parent, a_repr, b_repr);
    R::from_repr(repr, a_parent)
}

#[track_caller]
#[inline]
fn binop_mut<'p, 'q, T, R, U, A, B, F>(mut a: A, b: B, f: F) -> R
where
    T: Structure + 'p + 'q,
    R: FromRepr<'p, T, U>,
    A: BorrowElemMut<'p, T>,
    B: BorrowElem<'q, T>,
    F: FnOnce(&T, &mut T::MutRepr, &T::Repr) -> U,
{
    let (a_repr, a_parent) = a.pair_mut();
    let (b_repr, b_parent) = b.pair();

    assert!(a_parent == b_parent);

    let repr = f(a_parent, a_repr, b_repr);
    R::from_repr(repr, a_parent)
}

#[track_caller]
#[inline]
fn triop<'p, 'q, 'r, T, R, U, A, B, C, F>(a: A, b: B, c: C, f: F) -> R
where
    T: Structure + 'p + 'q + 'r,
    R: FromRepr<'p, T, U>,
    A: BorrowElem<'p, T>,
    B: BorrowElem<'q, T>,
    C: BorrowElem<'r, T>,
    F: FnOnce(&T, &T::Repr, &T::Repr, &T::Repr) -> U,
{
    let (a_repr, a_parent) = a.pair();
    let (b_repr, b_parent) = b.pair();
    let (c_repr, c_parent) = c.pair();

    assert!(a_parent == b_parent);
    assert!(a_parent == c_parent);

    let repr = f(a_parent, a_repr, b_repr, c_repr);
    R::from_repr(repr, a_parent)
}

#[track_caller]
#[inline]
fn triop_mut<'p, 'q, 'r, T, R, U, A, B, C, F>(mut a: A, b: B, c: C, f: F) -> R
where
    T: Structure + 'p + 'q + 'r,
    R: FromRepr<'p, T, U>,
    A: BorrowElemMut<'p, T>,
    B: BorrowElem<'q, T>,
    C: BorrowElem<'r, T>,
    F: FnOnce(&T, &mut T::MutRepr, &T::Repr, &T::Repr) -> U,
{
    let (a_repr, a_parent) = a.pair_mut();
    let (b_repr, b_parent) = b.pair();
    let (c_repr, c_parent) = c.pair();

    assert!(a_parent == b_parent);
    assert!(a_parent == c_parent);

    let repr = f(a_parent, a_repr, b_repr, c_repr);
    R::from_repr(repr, a_parent)
}

/// A borrowed element of an algebraic structure.
///
/// The representation of the element is borrowed with lifetime `'a` while the
/// parent of element (i.e., the algebraic structure) is borrowed with lifetime
/// `'p`.
///
/// # Panics
///
/// Every operation on an element panics if its parent differs from the parents
/// of the other operands.
pub trait Element<'a, 'p, T>: BorrowElem<'p, T> + Sized
where
    T: Structure + 'p,
    T::Repr: 'a,
{
    /// Returns the representation and parent (algebraic structure) of `self`.
    fn pair(self) -> (&'a T::Repr, &'p T);

    /// Returns the parent (algebraic structure) of `self`.
    #[inline]
    fn parent(self) -> &'p T {
        self.pair().1
    }

    /// Returns the representation of `self`.
    #[inline]
    fn repr(self) -> &'a T::Repr {
        self.pair().0
    }

    /// Converts `self` to an [`ElemRef`].
    #[inline]
    fn as_ref(self) -> ElemRef<'a, 'p, T> {
        let (repr, parent) = self.pair();
        parent.wrap_ref_unchecked(repr)
    }

    /// Converts `self` to an [`Elem`] by cloning.
    #[inline]
    fn to_elem(self) -> Elem<'p, T> {
        let (repr, parent) = self.pair();
        parent.wrap_unchecked(repr.clone_borrowed())
    }

    ////////////////////////////////////////////////////////////////////
    // Ring
    ////////////////////////////////////////////////////////////////////

    /// Returns true if `self` is the zero element.
    #[inline]
    fn is_zero(self) -> bool
    where
        T: Ring,
    {
        unop(self, T::repr_is_zero)
    }

    /// Returns true if `self` is the one element.
    #[inline]
    fn is_one(self) -> bool
    where
        T: Ring,
    {
        unop(self, T::repr_is_one)
    }

    /// Returns `self + other`.
    #[inline]
    fn add<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: Ring + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_add)
    }

    /// Returns `self - other`.
    #[inline]
    fn sub<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: Ring + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_sub)
    }

    /// Returns `self * other`.
    #[inline]
    fn mul<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: Ring + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_mul)
    }

    /// Returns `-self`.
    #[inline]
    fn neg(self) -> Elem<'p, T>
    where
        T: Ring,
    {
        unop(self, T::repr_neg)
    }

    /// Returns `self * count`.
    #[inline]
    fn mul_nat<N>(self, count: N) -> Elem<'p, T>
    where
        T: Ring,
        N: Natural,
    {
        let (repr, parent) = self.pair();
        let repr = parent.repr_mul_nat(repr, count);

        parent.wrap_unchecked(repr)
    }

    /// Returns `self * (-count)`.
    #[inline]
    fn mul_neg<N>(self, count: N) -> Elem<'p, T>
    where
        T: Ring,
        N: Natural,
    {
        let (repr, parent) = self.pair();
        let repr = parent.repr_mul_nat(repr, count);

        parent.wrap_unchecked(repr)
    }

    /// Returns `pow(self, count)`.
    #[inline]
    fn pow_nat<N>(self, count: N) -> Elem<'p, T>
    where
        T: Ring,
        N: Natural,
    {
        let (repr, parent) = self.pair();
        let repr = parent.repr_pow_nat(repr, count);

        parent.wrap_unchecked(repr)
    }

    ////////////////////////////////////////////////////////////////////
    // Scale
    ////////////////////////////////////////////////////////////////////

    /// Returns `self * scalar`.
    #[inline]
    fn scale<'q, U, E>(self, scalar: E) -> Elem<'p, T>
    where
        T: Scale<U>,
        U: Structure + 'q,
        E: BorrowElem<'q, U>,
    {
        let (a_repr, a_parent) = self.pair();
        let (b_repr, b_parent) = scalar.pair();
        let repr = a_parent.repr_scale(b_parent, a_repr, b_repr);

        a_parent.wrap_unchecked(repr)
    }

    ////////////////////////////////////////////////////////////////////
    // ReprPartialDiv
    ////////////////////////////////////////////////////////////////////

    /// Returns `self * inv(other)` if `other` is invertible.
    #[inline]
    fn partial_div<'q, E>(self, other: E) -> Option<Elem<'p, T>>
    where
        T: ReprPartialDiv + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_partial_div)
    }

    /// Returns `inv(other) * self` if `other` is invertible.
    #[inline]
    fn partial_left_div<'q, E>(self, other: E) -> Option<Elem<'p, T>>
    where
        T: ReprPartialDiv + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_partial_left_div)
    }

    /// Returns `inv(self)` if `self` is invertible.
    #[inline]
    fn partial_inv(self) -> Option<Elem<'p, T>>
    where
        T: ReprPartialDiv,
    {
        unop(self, T::repr_partial_inv)
    }

    /// Returns true if `self` is invertible.
    #[inline]
    fn is_unit(self) -> bool
    where
        T: ReprPartialDiv,
    {
        unop(self, T::repr_is_unit)
    }

    ////////////////////////////////////////////////////////////////////
    // DivisionRing
    ////////////////////////////////////////////////////////////////////

    /// Returns `self * inv(other)`.
    ///
    /// # Panics
    ///
    /// Panics if `other` is zero and the ring isn't trivial.
    #[inline]
    fn div<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: DivisionRing + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_div)
    }

    /// Returns `inv(other) * self`.
    ///
    /// # Panics
    ///
    /// Panics if `other` is zero and the ring isn't trivial.
    #[inline]
    fn left_div<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: DivisionRing + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_left_div)
    }

    /// Returns `inv(self)`.
    ///
    /// # Panics
    ///
    /// Panics if `self` is zero and the ring isn't trivial.
    #[inline]
    fn inv(self) -> Elem<'p, T>
    where
        T: DivisionRing,
    {
        unop(self, T::repr_inv)
    }

    ////////////////////////////////////////////////////////////////////
    // QuoRem
    ////////////////////////////////////////////////////////////////////

    /// Returns the quotient and remainder of `self` divided by `other`.
    #[inline]
    fn quo_rem<'q, E>(self, other: E) -> (Elem<'p, T>, Elem<'p, T>)
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_quo_rem)
    }

    /// Returns the quotient of `self` divided by `other`.
    #[inline]
    fn quo<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_quo)
    }

    /// Returns the remainder of `self` divided by `other`.
    #[inline]
    fn rem<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_rem)
    }

    /// Returns the remainder of `self * count` modulo `modulus`.
    #[inline]
    fn mul_nat_rem<'q, N, M>(self, count: N, modulus: M) -> Elem<'p, T>
    where
        T: QuoRem + 'q,
        N: Natural,
        M: BorrowElem<'q, T>,
    {
        let (a_repr, a_parent) = self.pair();
        let (b_repr, b_parent) = modulus.pair();

        assert!(a_parent == b_parent);

        let repr = a_parent.repr_mul_nat_rem(a_repr, count, b_repr);

        a_parent.wrap_unchecked(repr)
    }

    /// Returns the remainder of `self * (-count)` modulo `modulus`.
    #[inline]
    fn mul_neg_rem<'q, N, M>(self, count: N, modulus: M) -> Elem<'p, T>
    where
        T: QuoRem + 'q,
        N: Natural,
        M: BorrowElem<'q, T>,
    {
        let (a_repr, a_parent) = self.pair();
        let (b_repr, b_parent) = modulus.pair();

        assert!(a_parent == b_parent);

        let repr = a_parent.repr_mul_neg_rem(a_repr, count, b_repr);

        a_parent.wrap_unchecked(repr)
    }

    /// Returns the remainder of `pow(self, count)` modulo `modulus`.
    #[inline]
    fn pow_nat_rem<'q, N, M>(self, count: N, modulus: M) -> Elem<'p, T>
    where
        T: QuoRem + 'q,
        N: Natural,
        M: BorrowElem<'q, T>,
    {
        let (a_repr, a_parent) = self.pair();
        let (b_repr, b_parent) = modulus.pair();

        assert!(a_parent == b_parent);

        let repr = a_parent.repr_pow_nat_rem(a_repr, count, b_repr);

        a_parent.wrap_unchecked(repr)
    }

    /// Returns true if `self` and `other` are equivalent modulo `modulus`.
    #[inline]
    fn equiv_mod<'q, 'r, E, M>(self, other: E, modulus: M) -> bool
    where
        T: QuoRem + 'q + 'r,
        E: BorrowElem<'q, T>,
        M: BorrowElem<'r, T>,
    {
        triop(self, other, modulus, T::repr_equiv_mod)
    }

    /// Returns true if `self` is equivalent to zero modulo `modulus`.
    #[inline]
    fn is_zero_mod<'q, M>(self, modulus: M) -> bool
    where
        T: QuoRem + 'q,
        M: BorrowElem<'q, T>,
    {
        binop(self, modulus, T::repr_is_zero_mod)
    }

    /// Returns true if `self` is equivalent to one modulo `modulus`.
    #[inline]
    fn is_one_mod<'q, M>(self, modulus: M) -> bool
    where
        T: QuoRem + 'q,
        M: BorrowElem<'q, T>,
    {
        binop(self, modulus, T::repr_is_one_mod)
    }

    /// Returns true if `self` divides `other`.
    #[inline]
    fn divides<'q, E>(self, other: E) -> bool
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_divides)
    }

    /// Returns the greatest common divisor (GCD) of `self` and `other`.
    #[inline]
    fn gcd<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_gcd)
    }

    /// Returns the greatest common divisor (GCD) and minimal Bézout
    /// coefficients of `self` and `other`.
    ///
    /// This function returns `(d, s, t)` such that `self * s + other * t = d`.
    #[inline]
    fn gcd_ext<'q, E>(self, other: E) -> (Elem<'p, T>, Elem<'p, T>, Elem<'p, T>)
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop(self, other, T::repr_gcd_ext)
    }

    /// Returns `self * inv(other)` modulo `modulus` if `other` is invertible
    /// modulo `modulus`.
    #[inline]
    fn partial_div_mod<'q, 'r, E, M>(self, other: E, modulus: M) -> Option<Elem<'p, T>>
    where
        T: QuoRem + 'q + 'r,
        E: BorrowElem<'q, T>,
        M: BorrowElem<'r, T>,
    {
        triop(self, other, modulus, T::repr_partial_div_mod)
    }

    /// Returns `inv(self)` modulo `modulus` if `self` is invertible modulo
    /// `modulus`.
    #[inline]
    fn partial_inv_mod<'q, M>(self, modulus: M) -> Option<Elem<'p, T>>
    where
        T: QuoRem + 'q,
        M: BorrowElem<'q, T>,
    {
        binop(self, modulus, T::repr_partial_inv_mod)
    }

    /// Returns true if `self` is invertible modulo `modulus`.
    #[inline]
    fn is_unit_mod<'q, M>(self, modulus: M) -> bool
    where
        T: QuoRem + 'q,
        M: BorrowElem<'q, T>,
    {
        binop(self, modulus, T::repr_is_unit_mod)
    }
}

/// A mutably borrowed element of an algebraic structure.
///
/// The representation of the element is mutably borrowed with lifetime `'a`
/// while the parent of element (i.e., the algebraic structure) is (immutably)
/// borrowed with lifetime `'p`.
///
/// # Panics
///
/// Every operation on an element panics if its parent differs from the parents
/// of the other operands.
pub trait ElementMut<'a, 'p, T>: Element<'a, 'p, T> + BorrowElemMut<'p, T>
where
    T: Structure + 'p,
    T::Repr: 'a,
    T::MutRepr: 'a,
{
    /// Returns the representation and parent (algebraic structure) of `self`.
    fn pair_mut(self) -> (&'a mut T::MutRepr, &'p T);

    /// Returns the representation of `self` mutably.
    #[inline]
    fn repr_mut(self) -> &'a mut T::MutRepr {
        self.pair_mut().0
    }

    /// Converts `self` into an [`ElemMut`].
    #[inline]
    fn as_mut(self) -> ElemMut<'a, 'p, T> {
        let (repr, parent) = self.pair_mut();
        parent.wrap_mut_unchecked(repr)
    }

    /// Creates a "by reference" adaptor for this instance of `ElementMut`.
    ///
    /// The returned adaptor also implements `ElementMut` by simply mutably
    /// borrowing `self`.
    #[inline]
    fn by_ref(&mut self) -> &mut Self {
        self
    }

    ////////////////////////////////////////////////////////////////////
    // Ring
    ////////////////////////////////////////////////////////////////////

    /// Calculates `self = self + other`.
    #[inline]
    fn add_mut<'q, E>(self, other: E)
    where
        T: Ring + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_add_mut)
    }

    /// Calculates `self = self - other`.
    #[inline]
    fn sub_mut<'q, E>(self, other: E)
    where
        T: Ring + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_sub_mut)
    }

    /// Calculates `self = self * other`.
    #[inline]
    fn mul_mut<'q, E>(self, other: E)
    where
        T: Ring + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_mul_mut)
    }

    /// Calculates `self = other * self`.
    #[inline]
    fn left_mul_mut<'q, E>(self, other: E)
    where
        T: Ring + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_left_mul_mut)
    }

    /// Calculates `self = -self`.
    #[inline]
    fn neg_mut(self)
    where
        T: Ring,
    {
        unop_mut(self, T::repr_neg_mut)
    }

    /// Calculates `self = self + self`.
    #[inline]
    fn add_self_mut(self)
    where
        T: Ring,
    {
        unop_mut(self, T::repr_add_self_mut)
    }

    /// Calculates `self = self * self`.
    #[inline]
    fn mul_self_mut(self)
    where
        T: Ring,
    {
        unop_mut(self, T::repr_mul_self_mut)
    }

    /// Calculates `self = self + other * scale`.
    #[inline]
    fn add_mul_mut<'q, 'r, E, S>(self, other: E, scale: S)
    where
        T: Ring + 'q + 'r,
        E: BorrowElem<'q, T>,
        S: BorrowElem<'r, T>,
    {
        triop_mut(self, other, scale, T::repr_add_mul_mut)
    }

    /// Calculates `self = self - other * scale`.
    #[inline]
    fn sub_mul_mut<'q, 'r, E, S>(self, other: E, scale: S)
    where
        T: Ring + 'q + 'r,
        E: BorrowElem<'q, T>,
        S: BorrowElem<'r, T>,
    {
        triop_mut(self, other, scale, T::repr_sub_mul_mut)
    }

    /// Calculates `self = self * count`.
    #[inline]
    fn mul_nat_mut<N>(self, count: N)
    where
        T: Ring,
        N: Natural,
    {
        let (repr, parent) = self.pair_mut();
        parent.repr_mul_nat_mut(repr, count);
    }

    /// Calculates `self = self * (-count)`.
    #[inline]
    fn mul_neg_mut<N>(self, count: N)
    where
        T: Ring,
        N: Natural,
    {
        let (repr, parent) = self.pair_mut();
        parent.repr_mul_neg_mut(repr, count);
    }

    /// Calculates `self = pow(self, count)`.
    #[inline]
    fn pow_nat_mut<N>(self, count: N)
    where
        T: Ring,
        N: Natural,
    {
        let (repr, parent) = self.pair_mut();
        parent.repr_pow_nat_mut(repr, count);
    }

    ////////////////////////////////////////////////////////////////////
    // Scale
    ////////////////////////////////////////////////////////////////////

    /// Calculates `self = self * scalar`.
    #[inline]
    fn scale_mut<'q, U, E>(self, scalar: E)
    where
        T: Scale<U>,
        U: Structure + 'q,
        E: BorrowElem<'q, U>,
    {
        let (a_repr, a_parent) = self.pair_mut();
        let (b_repr, b_parent) = scalar.pair();
        a_parent.repr_scale_mut(b_parent, a_repr, b_repr);
    }

    /// Calculates `self = self + 1 * scalar` where `1` is the one element of
    /// the of ring of `self`.
    #[inline]
    fn add_scalar_mut<'q, U, E>(self, scalar: E)
    where
        T: Ring + Scale<U>,
        U: Structure + 'q,
        E: BorrowElem<'q, U>,
    {
        let (a_repr, a_parent) = self.pair_mut();
        let (b_repr, b_parent) = scalar.pair();
        a_parent.repr_add_scalar_mut(b_parent, a_repr, b_repr);
    }

    /// Calculates `self = self - 1 * scalar` where `1` is the one element of
    /// the of ring of `self`.
    #[inline]
    fn sub_scalar_mut<'q, U, E>(self, scalar: E)
    where
        T: Ring + Scale<U>,
        U: Structure + 'q,
        E: BorrowElem<'q, U>,
    {
        let (a_repr, a_parent) = self.pair_mut();
        let (b_repr, b_parent) = scalar.pair();
        a_parent.repr_sub_scalar_mut(b_parent, a_repr, b_repr);
    }

    ////////////////////////////////////////////////////////////////////
    // ReprPartialDiv
    ////////////////////////////////////////////////////////////////////

    /// Calculates `self = self * inv(other)` if `other` is invertible.
    #[inline]
    fn partial_div_mut<'q, E>(self, other: E) -> Result<(), ()>
    where
        T: ReprPartialDiv + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_partial_div_mut)
    }

    /// Calculates `self = inv(other) * self` if `other` is invertible.
    #[inline]
    fn partial_left_div_mut<'q, E>(self, other: E) -> Result<(), ()>
    where
        T: ReprPartialDiv + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_partial_left_div_mut)
    }

    ////////////////////////////////////////////////////////////////////
    // DivisionRing
    ////////////////////////////////////////////////////////////////////

    /// Calculates `self = self * inv(other)`.
    ///
    /// # Panics
    ///
    /// Panics if `other` is zero and the ring isn't trivial.
    #[inline]
    fn div_mut<'q, E>(self, other: E)
    where
        T: DivisionRing + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_div_mut)
    }

    /// Calculates `self = inv(other) * self`.
    ///
    /// # Panics
    ///
    /// Panics if `other` is zero and the ring isn't trivial.
    #[inline]
    fn left_div_mut<'q, E>(self, other: E)
    where
        T: DivisionRing + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_left_div_mut)
    }

    ////////////////////////////////////////////////////////////////////
    // QuoRem
    ////////////////////////////////////////////////////////////////////

    /// Calculates the quotient and remainder of `self` divided by `other`.
    ///
    /// The quotient is returned and `self` is overwritten by the remainder.
    #[inline]
    fn quo_rem_mut<'q, E>(self, other: E) -> Elem<'p, T>
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_quo_rem_mut)
    }

    /// Replaces `self` by the remainder of `self` divided by `other`.
    #[inline]
    fn rem_mut<'q, E>(self, other: E)
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_rem_mut)
    }

    /// Calculates `self = (self * count) rem modulus`.
    #[inline]
    fn mul_nat_rem_mut<'q, N, M>(self, count: N, modulus: M)
    where
        T: QuoRem + 'q,
        N: Natural,
        M: BorrowElem<'q, T>,
    {
        let (a_repr, a_parent) = self.pair_mut();
        let (m_repr, m_parent) = modulus.pair();

        assert!(a_parent == m_parent);

        a_parent.repr_mul_nat_rem_mut(a_repr, count, m_repr);
    }

    /// Calculates `self = (self * (-count)) rem modulus`.
    #[inline]
    fn mul_neg_rem_mut<'q, N, M>(self, count: N, modulus: M)
    where
        T: QuoRem + 'q,
        N: Natural,
        M: BorrowElem<'q, T>,
    {
        let (a_repr, a_parent) = self.pair_mut();
        let (m_repr, m_parent) = modulus.pair();

        assert!(a_parent == m_parent);

        a_parent.repr_mul_neg_rem_mut(a_repr, count, m_repr);
    }

    /// Calculates `self = pow(self, count) rem modulus`.
    #[inline]
    fn pow_nat_rem_mut<'q, N, M>(self, count: N, modulus: M)
    where
        T: QuoRem + 'q,
        N: Natural,
        M: BorrowElem<'q, T>,
    {
        let (a_repr, a_parent) = self.pair_mut();
        let (m_repr, m_parent) = modulus.pair();

        assert!(a_parent == m_parent);

        a_parent.repr_pow_nat_rem_mut(a_repr, count, m_repr);
    }

    /// Calculates `self = gcd(self, other)`.
    #[inline]
    fn gcd_mut<'q, E>(self, other: E)
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_gcd_mut)
    }

    /// Calculates `self = gcd(self, other)` and returns minimal Bézout
    /// coefficients.
    #[inline]
    fn gcd_ext_mut<'q, E>(self, other: E) -> (Elem<'p, T>, Elem<'p, T>)
    where
        T: QuoRem + 'q,
        E: BorrowElem<'q, T>,
    {
        binop_mut(self, other, T::repr_gcd_ext_mut)
    }

    /// Calculates `self = self * inv(other)` modulo `modulus` if `other` is
    /// invertible modulo `modulus`.
    ///
    /// If `other` is not invertible, `Err(())` is returned and `self` is left
    /// unchanged. Otherwise, `Ok(())` is returned and `self` contains the
    /// result.
    #[inline]
    fn partial_div_mod_mut<'q, 'r, E, M>(self, other: E, modulus: M) -> Result<(), ()>
    where
        T: QuoRem + 'q + 'r,
        E: BorrowElem<'q, T>,
        M: BorrowElem<'r, T>,
    {
        triop_mut(self, other, modulus, T::repr_partial_div_mod_mut)
    }
}

impl<'a, 'p, E, T> Element<'a, 'p, T> for &'a E
where
    E: BorrowElem<'p, T>,
    T: Structure + 'p,
    T::Repr: 'a,
{
    #[inline]
    fn pair(self) -> (&'a T::Repr, &'p T) {
        self.borrow_elem().pair()
    }
}

impl<'a, 'p, E, T> Element<'a, 'p, T> for &'a mut E
where
    E: BorrowElem<'p, T>,
    T: Structure + 'p,
    T::Repr: 'a,
{
    #[inline]
    fn pair(self) -> (&'a T::Repr, &'p T) {
        (*self).borrow_elem().pair()
    }
}

impl<'a, 'p, E, T> ElementMut<'a, 'p, T> for &'a mut E
where
    E: BorrowElemMut<'p, T>,
    T: Structure + 'p,
    T::Repr: 'a,
    T::MutRepr: 'a,
{
    #[inline]
    fn pair_mut(self) -> (&'a mut T::MutRepr, &'p T) {
        self.borrow_elem_mut().pair_mut()
    }
}

impl<'a, 'p, T> Element<'a, 'p, T> for ElemRef<'a, 'p, T>
where
    T: Structure + 'p,
{
    #[inline]
    fn pair(self) -> (&'a T::Repr, &'p T) {
        (self.repr, self.parent)
    }
}

impl<'a, 'p, T> Element<'a, 'p, T> for ElemMut<'a, 'p, T>
where
    T: Structure + 'p,
    T::Repr: 'a,
{
    #[inline]
    fn pair(self) -> (&'a T::Repr, &'p T) {
        ((*self.repr).borrow(), self.parent)
    }
}

impl<'a, 'p, T> ElementMut<'a, 'p, T> for ElemMut<'a, 'p, T>
where
    T: Structure + 'p,
    T::Repr: 'a,
{
    #[inline]
    fn pair_mut(self) -> (&'a mut T::MutRepr, &'p T) {
        (self.repr, self.parent)
    }
}

//// TODO
impl<'a, 'p, T> From<&ElemRef<'a, 'p, T>> for ElemRef<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: &ElemRef<'a, 'p, T>) -> Self {
        ElemRef::new_unchecked(other.repr, other.parent)
    }
}

impl<'a, 'p, T> From<&mut ElemRef<'a, 'p, T>> for ElemRef<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: &mut ElemRef<'a, 'p, T>) -> Self {
        ElemRef::new_unchecked(other.repr, other.parent)
    }
}

impl<'a, 'p, T> From<ElemMut<'a, 'p, T>> for ElemRef<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: ElemMut<'a, 'p, T>) -> Self {
        ElemRef::new_unchecked((*other.repr).borrow(), other.parent)
    }
}

impl<'a, 'p, T> From<&'a ElemMut<'_, 'p, T>> for ElemRef<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: &'a ElemMut<'_, 'p, T>) -> Self {
        ElemRef::new_unchecked((*other.repr).borrow(), other.parent)
    }
}

impl<'a, 'p, T> From<&'a mut ElemMut<'_, 'p, T>> for ElemRef<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: &'a mut ElemMut<'_, 'p, T>) -> Self {
        ElemRef::new_unchecked((*other.repr).borrow(), other.parent)
    }
}

impl<'a, 'p, T> From<&'a Elem<'p, T>> for ElemRef<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: &'a Elem<'p, T>) -> Self {
        ElemRef::new_unchecked(other.repr.borrow(), other.parent)
    }
}

impl<'a, 'p, T> From<&'a mut Elem<'p, T>> for ElemRef<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: &'a mut Elem<'p, T>) -> Self {
        ElemRef::new_unchecked(other.repr.borrow(), other.parent)
    }
}

impl<'a, 'p, T> From<&'a mut ElemMut<'_, 'p, T>> for ElemMut<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: &'a mut ElemMut<'_, 'p, T>) -> Self {
        ElemMut::new_unchecked(other.repr, other.parent)
    }
}

impl<'a, 'p, T> From<&'a mut Elem<'p, T>> for ElemMut<'a, 'p, T>
where
    T: Structure,
{
    fn from(other: &'a mut Elem<'p, T>) -> Self {
        ElemMut::new_unchecked(other.repr.borrow_mut(), other.parent)
    }
}
////

/// An owned element of a algebraic structure.
///
/// This element logically contains the representation of the element as an
/// [`T::OwnedRepr`][Structure::OwnedRepr] and the parent as a reference to `T`
/// with lifetime `'p`.
///
/// This type implements [`BorrowElem`] and [`BorrowElemMut`] and can thus be
/// used in calculations.
pub struct Elem<'p, T>
where
    T: Structure,
{
    repr: T::OwnedRepr,
    parent: &'p T,
}

impl<'p, T> Elem<'p, T>
where
    T: Structure,
{
    /// Returns an `Elem` with the given representation and parent.
    #[inline]
    pub fn new_unchecked(repr: T::OwnedRepr, parent: &'p T) -> Self {
        Elem { repr, parent }
    }

    /// Consumes `self` and returns the representation and parent.
    #[inline]
    pub fn into_pair(self) -> (T::OwnedRepr, &'p T) {
        (self.repr, self.parent)
    }

    /// Replaces the parent of `self` with `new_parent` to change the lifetime.
    ///
    /// # Panics
    ///
    /// Panics if `self.parent()` and `new_parent` are different.
    #[inline]
    pub fn with_parent(self, new_parent: &T) -> Elem<T> {
        let (repr, parent) = self.into_pair();
        assert!(new_parent == parent);
        Elem::new_unchecked(repr, new_parent)
    }
}

impl<T> Clone for Elem<'_, T>
where
    T: Structure,
{
    #[inline]
    fn clone(&self) -> Self {
        Elem {
            repr: self.repr.clone(),
            parent: self.parent,
        }
    }
}

impl<'a, T, U> PartialEq<U> for Elem<'_, T>
where
    T: ReprEq + 'a,
    U: BorrowElem<'a, T>,
{
    #[inline]
    fn eq(&self, other: &U) -> bool {
        let other = other.borrow_elem();
        assert!(self.parent() == other.parent());
        self.parent().repr_eq(self.repr(), other.repr())
    }
}

impl<T> Debug for Elem<'_, T>
where
    T: ReprDebug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.parent().fmt(self.repr(), f)
    }
}

/// A borrowed element of an algebraic structure.
///
/// This element logically contains the representation of the element as a
/// reference to a [`T::Repr`][Structure::Repr] with lifetime `'a` and the
/// parent as a reference to `T` with lifetime `'p`.
///
/// This type implements [`Element`] and [`BorrowElem`] and can thus be used in
/// calculations.
pub struct ElemRef<'a, 'p, T>
where
    T: Structure,
{
    repr: &'a T::Repr,
    parent: &'p T,
}

impl<'a, 'p, T> ElemRef<'a, 'p, T>
where
    T: Structure,
{
    /// Returns an `ElemRef` with the given representation and parent.
    #[inline]
    pub fn new_unchecked(repr: &'a T::Repr, parent: &'p T) -> Self {
        ElemRef { repr, parent }
    }

    /// Replaces the parent of `self` with `new_parent` to change the lifetime.
    ///
    /// # Panics
    ///
    /// Panics if `self.parent()` and `new_parent` are different.
    #[inline]
    pub fn with_parent<'q>(self, new_parent: &'q T) -> ElemRef<'a, 'q, T> {
        let (repr, parent) = self.pair();
        assert!(new_parent == parent);
        ElemRef::new_unchecked(repr, new_parent)
    }
}

impl<T> Clone for ElemRef<'_, '_, T>
where
    T: Structure,
{
    #[inline]
    fn clone(&self) -> Self {
        ElemRef {
            repr: self.repr,
            parent: self.parent,
        }
    }
}

impl<T> Copy for ElemRef<'_, '_, T> where T: Structure {}

impl<'a, T, U> PartialEq<U> for ElemRef<'_, '_, T>
where
    T: ReprEq + 'a,
    U: BorrowElem<'a, T>,
{
    #[inline]
    fn eq(&self, other: &U) -> bool {
        let other = other.borrow_elem();
        assert!(self.parent() == other.parent());
        self.parent().repr_eq(self.repr(), other.repr())
    }
}

impl<T> Debug for ElemRef<'_, '_, T>
where
    T: ReprDebug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.parent().fmt(self.repr(), f)
    }
}

/// A mutably borrowed element of an algebraic structure.
///
/// This element logically contains the representation of the element as a
/// mutable reference to a [`T::MutRepr`][Structure::MutRepr] with lifetime `'a`
/// and the parent as a reference to `T` with lifetime `'p`.
///
/// This type implements [`Element`], [`ElementMut`], [`BorrowElem`] and
/// [`BorrowElemMut`] and can thus be used in calculations.
pub struct ElemMut<'a, 'p, T>
where
    T: Structure,
{
    repr: &'a mut T::MutRepr,
    parent: &'p T,
}

impl<'a, 'p, T> ElemMut<'a, 'p, T>
where
    T: Structure,
{
    /// Returns an `ElemMut` with the given representation and parent.
    #[inline]
    pub fn new_unchecked(repr: &'a mut T::MutRepr, parent: &'p T) -> Self {
        ElemMut { repr, parent }
    }

    /// Consumes `self` and returns its representation and parent.
    #[inline]
    pub fn into_pair(self) -> (&'a mut T::MutRepr, &'p T) {
        (self.repr, self.parent)
    }

    /// Replaces the parent of `self` with `new_parent` to change the lifetime.
    ///
    /// # Panics
    ///
    /// Panics if `self.parent()` and `new_parent` are different.
    #[inline]
    pub fn with_parent<'q>(self, new_parent: &'q T) -> ElemMut<'a, 'q, T> {
        let (repr, parent) = self.into_pair();
        assert!(new_parent.borrow() == parent.borrow());
        ElemMut::new_unchecked(repr, new_parent)
    }
}

impl<'a, T, U> PartialEq<U> for ElemMut<'_, '_, T>
where
    T: ReprEq + 'a,
    U: BorrowElem<'a, T>,
{
    #[inline]
    fn eq(&self, other: &U) -> bool {
        let other = other.borrow_elem();
        assert!(self.parent() == other.parent());
        self.parent().repr_eq(self.repr(), other.repr())
    }
}

impl<T> Debug for ElemMut<'_, '_, T>
where
    T: ReprDebug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.parent().fmt(self.repr(), f)
    }
}
