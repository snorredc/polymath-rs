//! Rings.
//!
//! This module contains traits for defining rings and division rings.
//!
//! The central traits are [`Ring`], [`ReprPartialDiv`] and [`DivisionRing`].

use std::borrow::{Borrow, BorrowMut};
use std::iter::FusedIterator;

use super::{repeat_mut, Assign, Elem, FromBorrowed, ReprEq, Structure};

/// A natural number including zero.
pub trait Natural {
    /// Returns the bit length of `self` that is `ceil(log2(n + 1))`.
    fn bit_len(&self) -> usize;

    /// Returns the `i`th bit.
    ///
    /// The least significant bit has index zero and the most significant
    /// bit has index `self.bit_len() - 1`.
    fn bit(&self, i: usize) -> bool;

    /// Returns an iterator over the bits in `self`.
    #[inline]
    fn bits(&self) -> Bits<Self> {
        Bits {
            number: self,
            i: 0,
            j: self.bit_len(),
        }
    }
}

/// Bit iterator.
///
/// Created by the [`bits`] method on [`Natural`].
///
/// [`bits`]: Natural::bits
pub struct Bits<'a, N: ?Sized> {
    number: &'a N,
    i: usize,
    j: usize,
}

impl<N> Iterator for Bits<'_, N>
where
    N: Natural + ?Sized,
{
    type Item = bool;

    #[inline]
    fn next(&mut self) -> Option<bool> {
        if self.i < self.j {
            let i = self.i;
            self.i = i + 1;
            Some(self.number.bit(i))
        } else {
            None
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.j - self.i, Some(self.j - self.i))
    }
}

impl<N> DoubleEndedIterator for Bits<'_, N>
where
    N: Natural + ?Sized,
{
    #[inline]
    fn next_back(&mut self) -> Option<bool> {
        if self.i < self.j {
            self.j -= 1;
            Some(self.number.bit(self.j))
        } else {
            None
        }
    }
}

impl<N> ExactSizeIterator for Bits<'_, N> where N: Natural + ?Sized {}

impl<N> FusedIterator for Bits<'_, N> where N: Natural + ?Sized {}

impl<N> Natural for &N
where
    N: Natural,
{
    #[inline]
    fn bit_len(&self) -> usize {
        (**self).bit_len()
    }

    #[inline]
    fn bit(&self, k: usize) -> bool {
        (**self).bit(k)
    }
}

impl Natural for usize {
    #[inline]
    fn bit_len(&self) -> usize {
        (0usize.leading_zeros() - self.leading_zeros()) as usize
    }

    #[inline]
    fn bit(&self, k: usize) -> bool {
        (*self >> k) & 1 != 0
    }
}

/// A ring. Not necessarily commutative.
///
/// Instances of types that implement this represent concrete rings.
pub trait Ring: ReprEq + Scale<Self> {
    /// Returns a representation of the zero element.
    fn repr_zero(&self) -> Self::OwnedRepr;

    /// Returns a representation of the one element.
    fn repr_one(&self) -> Self::OwnedRepr;

    /// Returns `repr + other`.
    fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr;

    /// Returns `repr - other`.
    fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr;

    /// Returns `repr * other`.
    fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr;

    /// Returns the zero element.
    #[inline]
    fn zero(&self) -> Elem<Self> {
        self.wrap_unchecked(self.repr_zero())
    }

    /// Returns the one element.
    #[inline]
    fn one(&self) -> Elem<Self> {
        self.wrap_unchecked(self.repr_one())
    }

    /// Returns true if the ring is trivial (contains only a single element).
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn is_trivial(&self) -> bool {
        self.repr_is_one(self.repr_zero().borrow())
    }

    /// Returns `-repr`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_neg(&self, repr: &Self::Repr) -> Self::OwnedRepr {
        let mut result = self.repr_zero();
        self.repr_sub_mut(result.borrow_mut(), repr);
        result
    }

    /// Calculates `repr = repr + other`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_add_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        let result = self.repr_add((*repr).borrow(), other);
        repr.assign(result);
    }

    /// Calculates `repr = repr - other`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_sub_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        let result = self.repr_sub((*repr).borrow(), other);
        repr.assign(result);
    }

    /// Calculates `repr = repr * other`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        let result = self.repr_mul((*repr).borrow(), other);
        repr.assign(result);
    }

    /// Calculates `repr = other * repr`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_left_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        let result = self.repr_mul(other, (*repr).borrow());
        repr.assign(result);
    }

    /// Calculates `repr = -repr`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_neg_mut(&self, repr: &mut Self::MutRepr) {
        let neg = self.repr_neg((*repr).borrow());
        repr.assign(neg);
    }

    /// Calculates `repr = repr + repr`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_add_self_mut(&self, repr: &mut Self::MutRepr) {
        let clone = Self::OwnedRepr::from_borrowed((*repr).borrow());
        self.repr_add_mut(repr, clone.borrow());
    }

    /// Calculates `repr = repr * repr`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_mul_self_mut(&self, repr: &mut Self::MutRepr) {
        let clone = Self::OwnedRepr::from_borrowed((*repr).borrow());
        self.repr_mul_mut(repr, clone.borrow());
    }

    /// Calculates `repr = repr + other * scale`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_add_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr, scale: &Self::Repr) {
        let product = self.repr_mul(other, scale);
        self.repr_add_mut(repr, product.borrow());
    }

    /// Calculates `repr = repr - other * scale`.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_sub_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr, scale: &Self::Repr) {
        let product = self.repr_mul(other, scale);
        self.repr_sub_mut(repr, product.borrow());
    }

    /// Returns `repr * count`.
    ///
    /// The provided implementation performs `O(log count)` additions.
    fn repr_mul_nat<N>(&self, repr: &Self::Repr, count: N) -> Self::OwnedRepr
    where
        N: Natural,
    {
        let mut result = self.repr_zero();
        repeat_mut(
            count,
            result.borrow_mut(),
            |state| self.repr_add_self_mut(state),
            |state| self.repr_add_mut(state, repr),
        );
        result
    }

    /// Returns `repr * (-count)`.
    ///
    /// The provided implementation performs `O(log count)` additions.
    fn repr_mul_neg<N>(&self, repr: &Self::Repr, count: N) -> Self::OwnedRepr
    where
        N: Natural,
    {
        let mut result = self.repr_zero();
        repeat_mut(
            count,
            result.borrow_mut(),
            |state| self.repr_add_self_mut(state),
            |state| self.repr_sub_mut(state, repr),
        );
        result
    }

    /// Returns `pow(repr, count)`.
    ///
    /// The provided implementation performs `O(log count)` multiplications.
    fn repr_pow_nat<N>(&self, repr: &Self::Repr, count: N) -> Self::OwnedRepr
    where
        N: Natural,
    {
        let mut result = self.repr_one();
        repeat_mut(
            count,
            result.borrow_mut(),
            |state| self.repr_mul_self_mut(state),
            |state| self.repr_mul_mut(state, repr),
        );
        result
    }

    /// Calculates `repr = repr * count`.
    ///
    /// The provided implementation calls [`repr_mul_nat`] and costs a clone.
    ///
    /// [`repr_mul_nat`]: Ring::repr_mul_nat
    #[inline]
    fn repr_mul_nat_mut<N>(&self, repr: &mut Self::MutRepr, count: N)
    where
        N: Natural,
    {
        let result = self.repr_mul_nat((*repr).borrow(), count);
        repr.assign(result);
    }

    /// Calculates `repr = repr * (-count)`.
    ///
    /// The provided implementation calls [`repr_mul_neg`] and costs a clone.
    ///
    /// [`repr_mul_neg`]: Ring::repr_mul_neg
    #[inline]
    fn repr_mul_neg_mut<N>(&self, repr: &mut Self::MutRepr, count: N)
    where
        N: Natural,
    {
        let result = self.repr_mul_neg((*repr).borrow(), count);
        repr.assign(result);
    }

    /// Calculates `repr = pow(repr, count)`.
    ///
    /// The provided implementation calls [`repr_pow_nat`] and costs a clone.
    ///
    /// [`repr_pow_nat`]: Ring::repr_pow_nat
    #[inline]
    fn repr_pow_nat_mut<N>(&self, repr: &mut Self::MutRepr, count: N)
    where
        N: Natural,
    {
        let result = self.repr_pow_nat((*repr).borrow(), count);
        repr.assign(result);
    }

    /// Returns true if `repr` represents the zero element.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_is_zero(&self, repr: &Self::Repr) -> bool {
        self.repr_eq(repr, self.repr_zero().borrow())
    }

    /// Returns true if `repr` represents the one element.
    ///
    /// The provided implementation costs a clone.
    #[inline]
    fn repr_is_one(&self, repr: &Self::Repr) -> bool {
        self.repr_eq(repr, self.repr_one().borrow())
    }

    /// Returns `self` scaled by `other` seen as an element of `other_ring`.
    ///
    /// This is equivalent to [`Ring::repr_mul`] when `other_ring == self`. It
    /// is only necessary to override the provided implementation if other
    /// values of `other_ring` are supported.
    ///
    /// This is used to derive [`Scale::repr_scale`].
    #[inline]
    fn repr_ring_scale(
        &self,
        other_ring: &Self,
        repr: &Self::Repr,
        other: &Self::Repr,
    ) -> Self::OwnedRepr {
        assert!(*self == *other_ring);
        self.repr_mul(repr, other)
    }

    /// Calculates `self` scaled by `other` seen as an element of `other_ring`
    /// and writes the result to `self`.
    ///
    /// This is equivalent to [`Ring::repr_mul_mut`] when `other_ring == self`.
    /// It is only necessary to override the provided implementation if other
    /// values of `other_ring` are supported.
    ///
    /// This is used to derive [`Scale::repr_scale_mut`].
    #[inline]
    fn repr_ring_scale_mut(&self, other_ring: &Self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        assert!(*self == *other_ring);
        self.repr_mul_mut(repr, other)
    }

    /// Adds one scaled by `other` seen as an element of `other_ring` to `self`.
    ///
    /// This is equivalent to [`Ring::repr_add_mut`] when `other_ring == self`.
    /// It is only necessary to override the provided implementation if other
    /// values of `other_ring` are supported.
    ///
    /// This is used to derive [`Scale::repr_add_scalar_mut`].
    #[inline]
    fn repr_ring_add_scalar_mut(
        &self,
        other_ring: &Self,
        repr: &mut Self::MutRepr,
        other: &Self::Repr,
    ) {
        assert!(*self == *other_ring);
        self.repr_add_mut(repr, other);
    }

    /// Subtracts one scaled by `other` seen as an element of `other_ring` from
    /// `self`.
    ///
    /// This is equivalent to [`Ring::repr_sub_mut`] when `other_ring == self`.
    /// It is only necessary to override the provided implementation if other
    /// values of `other_ring` are supported.
    ///
    /// This is used to derive [`Scale::repr_sub_scalar_mut`].
    #[inline]
    fn repr_ring_sub_scalar_mut(
        &self,
        other_ring: &Self,
        repr: &mut Self::MutRepr,
        other: &Self::Repr,
    ) {
        assert!(*self == *other_ring);
        self.repr_sub_mut(repr, other);
    }
}

/// A commutative ring.
pub trait CommutRing: Ring {}

/// A ring with division defined for invertible elements.
pub trait ReprPartialDiv: Ring {
    /// Returns `repr * inv(other)` if `other` is invertible.
    fn repr_partial_div(&self, repr: &Self::Repr, other: &Self::Repr) -> Option<Self::OwnedRepr>;

    /// Returns `inv(other) * repr` if `other` is invertible.
    fn repr_partial_left_div(
        &self,
        repr: &Self::Repr,
        other: &Self::Repr,
    ) -> Option<Self::OwnedRepr>;

    /// Returns `inv(repr)` if `repr` is invertible.
    #[inline]
    fn repr_partial_inv(&self, repr: &Self::Repr) -> Option<Self::OwnedRepr> {
        self.repr_partial_div(self.repr_one().borrow(), repr)
    }

    /// Calculates `repr = repr * inv(other)` if `other` is invertible.
    #[inline]
    fn repr_partial_div_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) -> Result<(), ()> {
        let result = self.repr_partial_div((*repr).borrow(), other).ok_or(())?;
        repr.assign(result);
        Ok(())
    }

    /// Calculates `repr = inv(other) * repr` if `other` is invertible.
    #[inline]
    fn repr_partial_left_div_mut(
        &self,
        repr: &mut Self::MutRepr,
        other: &Self::Repr,
    ) -> Result<(), ()> {
        let result = self
            .repr_partial_left_div((*repr).borrow(), other)
            .ok_or(())?;
        repr.assign(result);
        Ok(())
    }

    /// Returns true if `repr` is invertible.
    #[inline]
    fn repr_is_unit(&self, repr: &Self::Repr) -> bool {
        self.repr_partial_inv(repr).is_some()
    }
}

/// A division ring. This is a ring where all elements but zero are invertible.
pub trait DivisionRing: ReprPartialDiv {
    /// Returns `repr * inv(other)`.
    ///
    /// # Panics
    ///
    /// Panics if `other` is zero and `self` isn't a trivial ring.
    #[inline]
    fn repr_div(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        self.repr_partial_div(repr, other).unwrap()
    }

    /// Returns `inv(other) * repr`.
    ///
    /// # Panics
    ///
    /// Panics if `other` is zero and `self` isn't a trivial ring.
    #[inline]
    fn repr_left_div(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        self.repr_partial_left_div(repr, other).unwrap()
    }

    /// Returns `inv(repr)`.
    ///
    /// # Panics
    ///
    /// Panics if `repr` is zero and `self` isn't a trivial ring.
    #[inline]
    fn repr_inv(&self, repr: &Self::Repr) -> Self::OwnedRepr {
        self.repr_partial_inv(repr).unwrap()
    }

    /// Calculates `repr = repr * inv(other)`.
    ///
    /// # Panics
    ///
    /// Panics if `other` is zero and `self` isn't a trivial ring.
    #[inline]
    fn repr_div_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        self.repr_partial_div_mut(repr, other).unwrap();
    }

    /// Calculates `repr = inv(other) * repr`.
    ///
    /// # Panics
    ///
    /// Panics if `other` is zero and `self` isn't a trivial ring.
    #[inline]
    fn repr_left_div_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        self.repr_partial_left_div_mut(repr, other).unwrap();
    }
}

/// A module over `T`. This is a structure that can be scaled by elements from
/// `T`.
///
/// All ring elements are automatically scalable by other elements from the same
/// ring.
pub trait Scale<T>: Structure
where
    T: Structure,
{
    /// Returns `repr` scaled by `scalar` interpreted as an element from
    /// `scalar_ring`.
    fn repr_scale(&self, scalar_ring: &T, repr: &Self::Repr, scalar: &T::Repr) -> Self::OwnedRepr;

    /// Calculates `repr` scaled by `scalar` interpreted as an element from
    /// `scalar_ring` and replaces `repr` by this.
    #[inline]
    fn repr_scale_mut(&self, scalar_ring: &T, repr: &mut Self::MutRepr, scalar: &T::Repr) {
        let result = self.repr_scale(scalar_ring, (*repr).borrow(), scalar);
        repr.assign(result);
    }

    /// Adds one scaled by `scalar` to `self` mutably.
    #[inline]
    fn repr_add_scalar_mut(&self, scalar_ring: &T, repr: &mut Self::MutRepr, scalar: &T::Repr)
    where
        Self: Ring,
    {
        let mut product = self.repr_one();
        self.repr_scale_mut(scalar_ring, product.borrow_mut(), scalar);
        self.repr_add_mut(repr, product.borrow());
    }

    /// Subtracts one scaled by `scalar` from `self` mutably.
    #[inline]
    fn repr_sub_scalar_mut(&self, scalar_ring: &T, repr: &mut Self::MutRepr, scalar: &T::Repr)
    where
        Self: Ring,
    {
        let mut product = self.repr_one();
        self.repr_scale_mut(scalar_ring, product.borrow_mut(), scalar);
        self.repr_sub_mut(repr, product.borrow());
    }
}

impl<T> Scale<T> for T
where
    T: Ring,
{
    #[inline]
    fn repr_scale(&self, scalar_ring: &T, repr: &Self::Repr, scalar: &T::Repr) -> Self::OwnedRepr {
        self.repr_ring_scale(scalar_ring, repr, scalar)
    }

    #[inline]
    fn repr_scale_mut(&self, scalar_ring: &T, repr: &mut Self::MutRepr, scalar: &T::Repr) {
        self.repr_ring_scale_mut(scalar_ring, repr, scalar);
    }

    #[inline]
    fn repr_add_scalar_mut(&self, scalar_ring: &T, repr: &mut Self::MutRepr, scalar: &T::Repr) {
        self.repr_ring_add_scalar_mut(scalar_ring, repr, scalar);
    }

    #[inline]
    fn repr_sub_scalar_mut(&self, scalar_ring: &T, repr: &mut Self::MutRepr, scalar: &T::Repr) {
        self.repr_ring_sub_scalar_mut(scalar_ring, repr, scalar);
    }
}

/// A commutative module over `T`.
// TODO: Explain what this means/requires.
pub trait CommutScale<T>: Scale<T>
where
    T: Structure,
{
}

impl<T> CommutScale<T> for T where T: CommutRing {}
