//! Square matrix rings.
//!
//! The central type is [`SquareMats`].
//!
//! The extension traits [`SquareMatsElement`] and [`SquareMatsElementMut`]
//! provide additional functions for elements over `SquareMats`.

use std::borrow::{Borrow, BorrowMut};
use std::fmt;

use super::{
    Elem, ElemMut, ElemRef, Element, ElementMut, ReprDebug, ReprEq, Ring, Scale, Structure,
};

// Note: Column-major representation.

/// Ring of square matrices with entries from the ring `T`.
///
/// The matrices are represented as a dense array of entries in column-major
/// order. Each entry is indexed using a pair of `usize`s such that `(col, row)`
/// refers to the `row`th row of the `col`th column starting at zero. Note, that
/// this differs from the usual mathematical notation in that the indices are
/// swapped.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct SquareMats<T> {
    base_ring: T,
    size: usize,
}

impl<T> SquareMats<T> {
    /// Returns a ring of `size × size` square matrices over `T`.
    #[inline]
    pub const fn new(base_ring: T, size: usize) -> Self {
        SquareMats { base_ring, size }
    }

    /// Returns the number of columns which is also the number of rows.
    #[inline]
    pub fn size(&self) -> usize {
        self.size
    }

    /// Returns the ring this ring is defined over.
    #[inline]
    pub fn base_ring(&self) -> &T {
        &self.base_ring
    }
}

impl<T> SquareMats<T>
where
    T: Structure,
{
    /// Returns a raw representation of a square matrix with the `(col, row)`
    /// entry set to `f(col, row)`.
    #[inline]
    pub fn repr_with<F>(&self, mut f: F) -> Box<[T::OwnedRepr]>
    where
        F: FnMut(usize, usize) -> T::OwnedRepr,
    {
        let mut result = Vec::with_capacity(self.size * self.size);
        for i in 0..self.size {
            for j in 0..self.size {
                result.push(f(i, j));
            }
        }
        result.into_boxed_slice()
    }

    /// Returns a square matrix with the `(col, row)` entry set to
    /// `f(col, row)`.
    #[inline]
    pub fn elem_with<'q, F>(&self, mut f: F) -> Elem<Self>
    where
        T: 'q,
        F: FnMut(usize, usize) -> Elem<'q, T>,
    {
        let repr = self.repr_with(|i, j| {
            let elem = f(i, j);
            let (repr, parent) = elem.into_pair();
            assert!(self.base_ring == *parent);
            repr
        });
        self.wrap_unchecked(repr)
    }

    /// Returns the representation of the transpose matrix of `repr`.
    #[inline]
    pub fn repr_transpose(&self, repr: &[T::OwnedRepr]) -> Box<[T::OwnedRepr]> {
        self.repr_with(|i, j| repr[self.size * j + i].clone())
    }
}

impl<T> Structure for SquareMats<T>
where
    T: Structure,
{
    type OwnedRepr = Box<[T::OwnedRepr]>;
    type MutRepr = [T::OwnedRepr];
    type Repr = [T::OwnedRepr];
}

/// An extension trait for references to elements over [`SquareMats`].
pub trait SquareMatsElement<'a, 'p, T>: Element<'a, 'p, SquareMats<T>>
where
    T: Structure + 'p,
    T::OwnedRepr: 'a,
{
    /// Returns the entry of the `i`th column and `j`th row.
    #[inline]
    fn entry(self, i: usize, j: usize) -> ElemRef<'a, 'p, T> {
        let (repr, parent) = self.pair();
        assert!(i < parent.size && j < parent.size);
        parent
            .base_ring
            .wrap_ref_unchecked(repr[i * parent.size + j].borrow())
    }

    /// Returns the transpose matrix of `self`.
    #[inline]
    fn transpose(self) -> Elem<'p, SquareMats<T>> {
        let (repr, parent) = self.pair();
        let repr = parent.repr_transpose(repr);
        parent.wrap_unchecked(repr)
    }
}

/// An extension trait for mutable references to elements over [`SquareMats`].
pub trait SquareMatsElementMut<'a, 'p, T>: ElementMut<'a, 'p, SquareMats<T>>
where
    T: Structure + 'p,
    T::OwnedRepr: 'a,
{
    /// Returns a mutable reference to the entry of the `i`th column and `j`th
    /// row.
    #[inline]
    fn entry_mut(self, i: usize, j: usize) -> ElemMut<'a, 'p, T> {
        let (repr, parent) = self.pair_mut();
        assert!(i < parent.size && j < parent.size);
        parent
            .base_ring
            .wrap_mut_unchecked(repr[i * parent.size + j].borrow_mut())
    }
}

impl<'a, 'p, T, E> SquareMatsElement<'a, 'p, T> for E
where
    E: Element<'a, 'p, SquareMats<T>>,
    T: Structure + 'p,
    T::OwnedRepr: 'a,
{
}

impl<'a, 'p, T, E> SquareMatsElementMut<'a, 'p, T> for E
where
    E: ElementMut<'a, 'p, SquareMats<T>>,
    T: Structure + 'p,
    T::OwnedRepr: 'a,
{
}

impl<T> ReprEq for SquareMats<T>
where
    T: ReprEq,
{
    #[inline]
    fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
        (0..self.size * self.size)
            .all(|i| self.base_ring.repr_eq(repr[i].borrow(), other[i].borrow()))
    }
}

impl<T> ReprDebug for SquareMats<T>
where
    T: ReprDebug,
{
    #[inline]
    fn fmt(&self, repr: &Self::Repr, f: &mut fmt::Formatter) -> fmt::Result {
        let mut list = f.debug_list();
        for repr in repr {
            let elem = self.base_ring.wrap_ref_unchecked(repr.borrow());
            list.entry(&elem);
        }
        list.finish()
    }
}

impl<T> Scale<T> for SquareMats<T>
where
    T: Ring,
{
    #[inline]
    fn repr_scale(&self, scalar_ring: &T, repr: &Self::Repr, scalar: &T::Repr) -> Self::OwnedRepr {
        assert!(self.base_ring == *scalar_ring);
        let mut result = Vec::with_capacity(self.size * self.size);
        for i in 0..self.size * self.size {
            result.push(self.base_ring.repr_mul(scalar, repr[i].borrow()));
        }
        result.into_boxed_slice()
    }
}

impl<T> Ring for SquareMats<T>
where
    T: Ring,
{
    #[inline]
    fn repr_zero(&self) -> Self::OwnedRepr {
        self.repr_with(|_, _| self.base_ring.repr_zero())
    }

    #[inline]
    fn repr_one(&self) -> Self::OwnedRepr {
        self.repr_with(|i, j| {
            if i == j {
                self.base_ring.repr_one()
            } else {
                self.base_ring.repr_zero()
            }
        })
    }

    #[inline]
    fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = Vec::with_capacity(self.size * self.size);
        for i in 0..self.size * self.size {
            result.push(self.base_ring.repr_add(repr[i].borrow(), other[i].borrow()));
        }
        result.into_boxed_slice()
    }

    #[inline]
    fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = Vec::with_capacity(self.size * self.size);
        for i in 0..self.size * self.size {
            result.push(self.base_ring.repr_sub(repr[i].borrow(), other[i].borrow()));
        }
        result.into_boxed_slice()
    }

    #[inline]
    fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = self.repr_zero();
        self.repr_add_mul_mut(result.borrow_mut(), repr, other);
        result
    }

    #[inline]
    fn repr_add_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        for i in 0..self.size * self.size {
            self.base_ring
                .repr_add_mut(repr[i].borrow_mut(), other[i].borrow());
        }
    }

    #[inline]
    fn repr_sub_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        for i in 0..self.size * self.size {
            self.base_ring
                .repr_sub_mut(repr[i].borrow_mut(), other[i].borrow());
        }
    }

    #[inline]
    fn repr_add_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr, scale: &Self::Repr) {
        for i in 0..self.size {
            for j in 0..self.size {
                for k in 0..self.size {
                    self.base_ring.repr_add_mul_mut(
                        repr[self.size * i + j].borrow_mut(),
                        other[self.size * k + j].borrow(),
                        scale[self.size * i + k].borrow(),
                    );
                }
            }
        }
    }

    #[inline]
    fn repr_sub_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr, scale: &Self::Repr) {
        for i in 0..self.size {
            for j in 0..self.size {
                for k in 0..self.size {
                    self.base_ring.repr_sub_mul_mut(
                        repr[self.size * i + j].borrow_mut(),
                        other[self.size * k + j].borrow(),
                        scale[self.size * i + k].borrow(),
                    );
                }
            }
        }
    }
}
