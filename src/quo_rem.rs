//! Euclidean domains, quotient/remainder, and quotient rings.

use std::borrow::{Borrow, BorrowMut};
use std::fmt::{self, Debug};

use super::{
    repeat_mut, Assign, BorrowElem, CommutRing, DivisionRing, Elem, ElemRef, Element, FromBorrowed,
    Natural, ReprDebug, ReprEq, ReprPartialDiv, Ring, Structure,
};

/// An Euclidean domain defined through quotient/remainder.
pub trait QuoRem: CommutRing {
    /// Returns the quotient and remainder of `repr` when dividing with `other`.
    fn repr_quo_rem(
        &self,
        repr: &Self::Repr,
        other: &Self::Repr,
    ) -> (Self::OwnedRepr, Self::OwnedRepr);

    /// Returns the quotient of `repr` when dividing with `other`.
    #[inline]
    fn repr_quo(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        self.repr_quo_rem(repr, other).0
    }

    /// Returns the remainder of `repr` when dividing with `other`.
    #[inline]
    fn repr_rem(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        self.repr_quo_rem(repr, other).1
    }

    /// Calculates the remainder of `repr` when dividing with `other` in-place
    /// and returns the quotient.
    #[inline]
    fn repr_quo_rem_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) -> Self::OwnedRepr {
        let (quo, rem) = self.repr_quo_rem((*repr).borrow(), other);
        repr.assign(rem);
        quo
    }

    /// Calculates the remainder of `repr` when dividing with `other` in-place.
    #[inline]
    fn repr_rem_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        let result = self.repr_rem((*repr).borrow(), other);
        repr.assign(result);
    }

    /// Returns `(repr * count) rem modulus`.
    ///
    /// The provided implementation uses adding and doubling, taking the
    /// remainder after each operation.
    #[inline]
    fn repr_mul_nat_rem<N>(
        &self,
        repr: &Self::Repr,
        count: N,
        modulus: &Self::Repr,
    ) -> Self::OwnedRepr
    where
        N: Natural,
    {
        let mut result = self.repr_zero();
        repeat_mut(
            count,
            result.borrow_mut(),
            |state| {
                self.repr_add_self_mut(state);
                self.repr_rem_mut(state, modulus);
            },
            |state| {
                self.repr_add_mut(state, repr);
                self.repr_rem_mut(state, modulus);
            },
        );
        result
    }

    /// Returns `(repr * (-count)) rem modulus`.
    ///
    /// The provided implementation uses subtracting and doubling, taking the
    /// remainder after each operation.
    #[inline]
    fn repr_mul_neg_rem<N>(
        &self,
        repr: &Self::Repr,
        count: N,
        modulus: &Self::Repr,
    ) -> Self::OwnedRepr
    where
        N: Natural,
    {
        let mut result = self.repr_zero();
        repeat_mut(
            count,
            result.borrow_mut(),
            |state| {
                self.repr_add_self_mut(state);
                self.repr_rem_mut(state, modulus);
            },
            |state| {
                self.repr_sub_mut(state, repr);
                self.repr_rem_mut(state, modulus);
            },
        );
        result
    }

    /// Returns `pow(repr, count) rem modulus`.
    ///
    /// The provided implementation uses multiplying and squaring, taking the
    /// remainder after each operation.
    #[inline]
    fn repr_pow_nat_rem<N>(
        &self,
        repr: &Self::Repr,
        count: N,
        modulus: &Self::Repr,
    ) -> Self::OwnedRepr
    where
        N: Natural,
    {
        let mut result = self.repr_zero();
        repeat_mut(
            count,
            result.borrow_mut(),
            |state| {
                self.repr_mul_self_mut(state);
                self.repr_rem_mut(state, modulus);
            },
            |state| {
                self.repr_mul_mut(state, repr);
                self.repr_rem_mut(state, modulus);
            },
        );
        result
    }

    /// Calculates [`QuoRem::repr_mul_nat_rem`] in-place.
    #[inline]
    fn repr_mul_nat_rem_mut<N>(&self, repr: &mut Self::MutRepr, count: N, modulus: &Self::Repr)
    where
        N: Natural,
    {
        let result = self.repr_mul_nat_rem((*repr).borrow(), count, modulus);
        repr.assign(result);
    }

    /// Calculates [`QuoRem::repr_mul_neg_rem`] in-place.
    #[inline]
    fn repr_mul_neg_rem_mut<N>(&self, repr: &mut Self::MutRepr, count: N, modulus: &Self::Repr)
    where
        N: Natural,
    {
        let result = self.repr_mul_neg_rem((*repr).borrow(), count, modulus);
        repr.assign(result);
    }

    /// Calculates [`QuoRem::repr_pow_nat_rem`] in-place.
    #[inline]
    fn repr_pow_nat_rem_mut<N>(&self, repr: &mut Self::MutRepr, count: N, modulus: &Self::Repr)
    where
        N: Natural,
    {
        let result = self.repr_pow_nat_rem((*repr).borrow(), count, modulus);
        repr.assign(result);
    }

    /// Returns true if `repr` and `other` are equivalent modulo `modulus`.
    #[inline]
    fn repr_equiv_mod(&self, repr: &Self::Repr, other: &Self::Repr, modulus: &Self::Repr) -> bool {
        let mut diff = self.repr_sub(repr, other);
        self.repr_rem_mut(diff.borrow_mut(), modulus);
        self.repr_is_zero(diff.borrow())
    }

    /// Returns true if `repr` is equivalent to zero modulo `modulus`.
    #[inline]
    fn repr_is_zero_mod(&self, repr: &Self::Repr, modulus: &Self::Repr) -> bool {
        self.repr_is_zero(self.repr_rem(repr, modulus).borrow())
    }

    /// Returns true if `repr` is equivalent to one modulo `modulus`.
    #[inline]
    fn repr_is_one_mod(&self, repr: &Self::Repr, modulus: &Self::Repr) -> bool {
        self.repr_is_one(self.repr_rem(repr, modulus).borrow())
    }

    /// Returns true if every element of `self` has the same remainder modulo
    /// `modulus`.
    ///
    /// This is equivalent to the quotient ring formed by `self` and the ideal
    /// generated by `modulus` being trivial.
    #[inline]
    fn repr_is_trivial_mod(&self, modulus: &Self::Repr) -> bool {
        self.repr_is_one_mod(self.repr_zero().borrow(), modulus)
    }

    /// Returns true if `repr` divides `other`.
    #[inline]
    fn repr_divides(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
        self.repr_is_zero_mod(other, repr)
    }

    /// Returns the greatest common divisor (GCD) of `repr` and `other`.
    #[inline]
    fn repr_gcd(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = Self::OwnedRepr::from_borrowed(repr);
        self.repr_gcd_mut(result.borrow_mut(), other);
        result
    }

    /// Calculates the greatest common divisor (GCD) of `repr` and `other`
    /// in-place.
    fn repr_gcd_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        if self.repr_is_zero(other) {
            return;
        }

        let mut t = Self::OwnedRepr::from_borrowed(other);

        let that = t.borrow_mut();

        loop {
            self.repr_rem_mut(repr, (*that).borrow());

            if self.repr_is_zero((*repr).borrow()) {
                repr.assign(t);
                break;
            }

            self.repr_rem_mut(that, (*repr).borrow());

            if self.repr_is_zero((*that).borrow()) {
                break;
            }
        }
    }

    /// Returns the greatest common divisor (GCD) and minimal Bézout
    /// coefficients of `repr` and `other`.
    ///
    /// This function returns `(d, s, t)` such that `repr * s + other * t = d`.
    #[inline]
    fn repr_gcd_ext(
        &self,
        repr: &Self::Repr,
        other: &Self::Repr,
    ) -> (Self::OwnedRepr, Self::OwnedRepr, Self::OwnedRepr) {
        let mut d = Self::OwnedRepr::from_borrowed(repr);
        let (s, t) = self.repr_gcd_ext_mut(d.borrow_mut(), other);
        (d, s, t)

        /*
        let mut r0 = Self::OwnedRepr::from_borrowed(repr);
        let mut s0 = self.repr_one();
        let mut t0 = self.repr_zero();

        let mut r1 = Self::OwnedRepr::from_borrowed(other);
        let mut s1 = self.repr_zero();
        let mut t1 = self.repr_one();

        while !self.repr_is_zero(r1.borrow()) {
            let (q, r) = self.repr_quo_rem(r0.borrow(), r1.borrow());

            let s = self.repr_sub(s0.borrow(), self.repr_mul(s1.borrow(), q.borrow()).borrow());
            let t = self.repr_sub(t0.borrow(), self.repr_mul(t1.borrow(), q.borrow()).borrow());

            r0 = mem::replace(&mut r1, r);
            s0 = mem::replace(&mut s1, s);
            t0 = mem::replace(&mut t1, t);
        }

        (r0, s0, t0)
        */
    }

    /// Calculates the greatest common divisor (GCD) of `repr` and `other`
    /// in-place and returns minimal Bézout coefficients.
    fn repr_gcd_ext_mut(
        &self,
        repr: &mut Self::MutRepr,
        other: &Self::Repr,
    ) -> (Self::OwnedRepr, Self::OwnedRepr) {
        let mut s0 = self.repr_one();
        let mut t0 = self.repr_zero();

        if self.repr_is_zero(other) {
            return (s0, t0);
        }

        let mut s1 = self.repr_zero();
        let mut t1 = self.repr_one();

        let mut t = Self::OwnedRepr::from_borrowed(other);

        let that = t.borrow_mut();

        loop {
            let q = self.repr_quo_rem_mut(repr, (*that).borrow());

            if self.repr_is_zero((*repr).borrow()) {
                repr.assign(t);
                return (s1, t1);
            }

            self.repr_sub_mul_mut(s0.borrow_mut(), s1.borrow(), q.borrow());
            self.repr_sub_mul_mut(t0.borrow_mut(), t1.borrow(), q.borrow());

            let q = self.repr_quo_rem_mut(that, (*repr).borrow());

            if self.repr_is_zero((*that).borrow()) {
                return (s0, t0);
            }

            self.repr_sub_mul_mut(s1.borrow_mut(), s0.borrow(), q.borrow());
            self.repr_sub_mul_mut(t1.borrow_mut(), t0.borrow(), q.borrow());
        }
    }

    /// Returns `repr * inv(other)` modulo `modulus` if `other` is invertible
    /// modulo `modulus`.
    #[inline]
    fn repr_partial_div_mod(
        &self,
        repr: &Self::Repr,
        other: &Self::Repr,
        modulus: &Self::Repr,
    ) -> Option<Self::OwnedRepr> {
        let (r, s, _) = self.repr_gcd_ext(other, modulus);

        if self.repr_is_one(r.borrow()) {
            let mut result = self.repr_mul(repr, s.borrow());
            self.repr_rem_mut(result.borrow_mut(), modulus);
            Some(result)
        } else {
            None
        }
    }

    /// Returns `inv(repr)` modulo `modulus` if `repr` is invertible modulo
    /// `modulus`.
    #[inline]
    fn repr_partial_inv_mod(
        &self,
        repr: &Self::Repr,
        modulus: &Self::Repr,
    ) -> Option<Self::OwnedRepr> {
        let (r, s, _) = self.repr_gcd_ext(repr, modulus);

        if self.repr_is_one(r.borrow()) {
            Some(self.repr_rem(s.borrow(), modulus))
        } else {
            None
        }
    }

    /// Calculates `repr = repr * inv(other)` modulo `modulus` if `other` is
    /// invertible modulo `modulus`.
    ///
    /// If `other` is not invertible, `Err(())` is returned and `self` is left
    /// unchanged. Otherwise, `Ok(())` is returned and `self` contains the
    /// result.
    #[inline]
    fn repr_partial_div_mod_mut(
        &self,
        repr: &mut Self::MutRepr,
        other: &Self::Repr,
        modulus: &Self::Repr,
    ) -> Result<(), ()> {
        let (d, s, _) = self.repr_gcd_ext(other, modulus);

        if !self.repr_is_one(d.borrow()) {
            return Err(());
        }

        self.repr_mul_mut(repr, s.borrow());
        self.repr_rem_mut(repr, modulus);

        Ok(())
    }

    /// Returns true if `self` is invertible modulo `modulus`.
    #[inline]
    fn repr_is_unit_mod(&self, repr: &Self::Repr, modulus: &Self::Repr) -> bool {
        let d = self.repr_gcd(repr, modulus);
        self.repr_is_one(d.borrow())
    }

    // Note: `repr_partial_left_div_mod` not needed because Euclidean domains are commutative.
}

/// A quotient ring over a given base ring and with a given modulus.
///
/// The first type parameter `T` gives the type of the base ring, while the
/// optional second type parameter `R` gives the type of the representation. For
/// `Quo<T, R>` to act as an ring, `R` must implement `Borrow<T::Repr>`. The
/// default value of `R` is the owned representation type of `T`,
/// `T::OwnedRepr`.
#[derive(Clone)]
pub struct Quo<T, R = <T as Structure>::OwnedRepr> {
    base_ring: T,
    modulus: R,
}

impl<T, R> Quo<T, R> {
    /// Creates a quotient ring over `base_ring` modulo `modulus`.
    #[inline]
    pub const fn new_unchecked(base_ring: T, modulus: R) -> Self {
        Quo { base_ring, modulus }
    }

    /// Returns the base ring of `self`.
    #[inline]
    pub fn base_ring(&self) -> &T {
        &self.base_ring
    }
}

impl<T> Quo<T>
where
    T: QuoRem,
{
    /// Converts an element from the base ring of `self` to an element of
    /// `self`.
    // TODO: Rename this or the "from repr" functions for consistency.
    pub fn wrap<'a, E>(&self, elem: E) -> Elem<Self>
    where
        T: 'a,
        E: BorrowElem<'a, T>,
    {
        let (repr, parent) = elem.borrow_elem().pair();

        assert!(*parent == self.base_ring);

        let repr = self
            .base_ring
            .repr_rem(repr.borrow(), self.modulus.borrow());

        self.wrap_unchecked(repr)
    }
}

impl<T, R> Quo<T, R>
where
    T: Structure,
    R: Borrow<T::Repr>,
{
    /// Returns the modulus of `self`.
    #[inline]
    pub fn modulus(&self) -> ElemRef<T> {
        self.base_ring.wrap_ref_unchecked(self.modulus.borrow())
    }

    /// Returns the representation of the modulus of `self`.
    #[inline]
    pub fn repr_modulus(&self) -> &T::Repr {
        self.modulus.borrow()
    }
}

impl<T> From<Elem<'_, T>> for Quo<T>
where
    T: Structure + Clone,
{
    fn from(other: Elem<T>) -> Self {
        let (repr, parent) = other.into_pair();
        Quo::new_unchecked(parent.clone(), repr)
    }
}

impl<T, R> PartialEq for Quo<T, R>
where
    T: ReprEq,
    R: Borrow<T::Repr>,
{
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.base_ring == other.base_ring
            && self
                .base_ring
                .repr_eq(self.modulus.borrow(), other.modulus.borrow())
    }
}

impl<T, R> Structure for Quo<T, R>
where
    T: ReprEq,
    R: Borrow<T::Repr>,
{
    type OwnedRepr = T::OwnedRepr;
    type MutRepr = T::MutRepr;
    type Repr = T::Repr;
}

impl<T, R> ReprEq for Quo<T, R>
where
    T: QuoRem,
    R: Borrow<T::Repr>,
{
    #[inline]
    fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
        // We are guaranteed that `repr` and `other` are reduced modulo
        // `self.modulus`, so it is sound to call `repr_eq` instead of
        // `repr_equiv_mod` here.
        self.base_ring.repr_eq(repr, other)
    }
}

impl<T, R> Debug for Quo<T, R>
where
    T: Debug + ReprDebug + ReprEq,
    R: Borrow<T::Repr>,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_tuple("Quo")
            .field(&self.base_ring)
            .field(&self.modulus())
            .finish()
    }
}

impl<T, R> ReprDebug for Quo<T, R>
where
    T: ReprDebug + ReprEq,
    R: Borrow<T::Repr>,
{
    fn fmt(&self, repr: &T::Repr, f: &mut fmt::Formatter) -> fmt::Result {
        self.base_ring.fmt(repr, f)
    }
}

impl<T, R> Ring for Quo<T, R>
where
    T: QuoRem,
    R: Borrow<T::Repr>,
{
    #[inline]
    fn repr_zero(&self) -> Self::OwnedRepr {
        self.base_ring.repr_zero()
    }

    #[inline]
    fn repr_one(&self) -> Self::OwnedRepr {
        if self.base_ring.repr_is_trivial_mod(self.modulus.borrow()) {
            self.base_ring.repr_zero()
        } else {
            self.base_ring.repr_one()
        }
    }

    #[inline]
    fn is_trivial(&self) -> bool {
        self.base_ring.repr_is_trivial_mod(self.modulus.borrow())
    }

    #[inline]
    fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = self.base_ring.repr_add(repr, other);
        self.base_ring
            .repr_rem_mut(result.borrow_mut(), self.modulus.borrow());
        result
    }

    #[inline]
    fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = self.base_ring.repr_sub(repr, other);
        self.base_ring
            .repr_rem_mut(result.borrow_mut(), self.modulus.borrow());
        result
    }

    #[inline]
    fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        let mut result = self.base_ring.repr_mul(repr, other);
        self.base_ring
            .repr_rem_mut(result.borrow_mut(), self.modulus.borrow());
        result
    }

    #[inline]
    fn repr_neg(&self, repr: &Self::Repr) -> Self::OwnedRepr {
        let mut result = self.base_ring.repr_neg(repr);
        self.base_ring
            .repr_rem_mut(result.borrow_mut(), self.modulus.borrow());
        result
    }

    #[inline]
    fn repr_add_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        self.base_ring.repr_add_mut(repr, other);
        self.base_ring.repr_rem_mut(repr, self.modulus.borrow());
    }

    #[inline]
    fn repr_sub_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        self.base_ring.repr_sub_mut(repr, other);
        self.base_ring.repr_rem_mut(repr, self.modulus.borrow());
    }

    #[inline]
    fn repr_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) {
        self.base_ring.repr_mul_mut(repr, other);
        self.base_ring.repr_rem_mut(repr, self.modulus.borrow());
    }

    #[inline]
    fn repr_neg_mut(&self, repr: &mut Self::MutRepr) {
        self.base_ring.repr_neg_mut(repr);
        self.base_ring.repr_rem_mut(repr, self.modulus.borrow());
    }

    #[inline]
    fn repr_add_self_mut(&self, repr: &mut Self::MutRepr) {
        self.base_ring.repr_add_self_mut(repr);
        self.base_ring.repr_rem_mut(repr, self.modulus.borrow());
    }

    #[inline]
    fn repr_mul_self_mut(&self, repr: &mut Self::MutRepr) {
        self.base_ring.repr_mul_self_mut(repr);
        self.base_ring.repr_rem_mut(repr, self.modulus.borrow());
    }

    #[inline]
    fn repr_add_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr, scale: &Self::Repr) {
        self.base_ring.repr_add_mul_mut(repr, other, scale);
        self.base_ring.repr_rem_mut(repr, self.modulus.borrow());
    }

    #[inline]
    fn repr_sub_mul_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr, scale: &Self::Repr) {
        self.base_ring.repr_sub_mul_mut(repr, other, scale);
        self.base_ring.repr_rem_mut(repr, self.modulus.borrow());
    }

    #[inline]
    fn repr_mul_nat<N>(&self, repr: &Self::Repr, count: N) -> Self::OwnedRepr
    where
        N: Natural,
    {
        self.base_ring
            .repr_mul_nat_rem(repr, count, self.modulus.borrow())
    }

    #[inline]
    fn repr_mul_neg<N>(&self, repr: &Self::Repr, count: N) -> Self::OwnedRepr
    where
        N: Natural,
    {
        self.base_ring
            .repr_mul_neg_rem(repr, count, self.modulus.borrow())
    }

    #[inline]
    fn repr_pow_nat<N>(&self, repr: &Self::Repr, count: N) -> Self::OwnedRepr
    where
        N: Natural,
    {
        self.base_ring
            .repr_pow_nat_rem(repr, count, self.modulus.borrow())
    }

    #[inline]
    fn repr_mul_nat_mut<N>(&self, repr: &mut Self::MutRepr, count: N)
    where
        N: Natural,
    {
        self.base_ring
            .repr_mul_nat_rem_mut(repr, count, self.modulus.borrow())
    }

    #[inline]
    fn repr_mul_neg_mut<N>(&self, repr: &mut Self::MutRepr, count: N)
    where
        N: Natural,
    {
        self.base_ring
            .repr_mul_neg_rem_mut(repr, count, self.modulus.borrow())
    }

    #[inline]
    fn repr_pow_nat_mut<N>(&self, repr: &mut Self::MutRepr, count: N)
    where
        N: Natural,
    {
        self.base_ring
            .repr_pow_nat_rem_mut(repr, count, self.modulus.borrow())
    }

    #[inline]
    fn repr_is_zero(&self, repr: &Self::Repr) -> bool {
        self.base_ring.repr_is_zero_mod(repr, self.modulus.borrow())
    }

    #[inline]
    fn repr_is_one(&self, repr: &Self::Repr) -> bool {
        self.base_ring.repr_is_one_mod(repr, self.modulus.borrow())
    }
}

impl<T, R> ReprPartialDiv for Quo<T, R>
where
    T: QuoRem,
    R: Borrow<T::Repr>,
{
    #[inline]
    fn repr_partial_div(&self, repr: &Self::Repr, other: &Self::Repr) -> Option<Self::OwnedRepr> {
        self.base_ring
            .repr_partial_div_mod(repr, other, self.modulus.borrow())
    }

    #[inline]
    fn repr_partial_left_div(
        &self,
        repr: &Self::Repr,
        other: &Self::Repr,
    ) -> Option<Self::OwnedRepr> {
        self.base_ring
            .repr_partial_div_mod(repr, other, self.modulus.borrow())
    }

    #[inline]
    fn repr_partial_inv(&self, repr: &Self::Repr) -> Option<Self::OwnedRepr> {
        self.base_ring
            .repr_partial_inv_mod(repr, self.modulus.borrow())
    }

    #[inline]
    fn repr_partial_div_mut(&self, repr: &mut Self::MutRepr, other: &Self::Repr) -> Result<(), ()> {
        self.base_ring
            .repr_partial_div_mod_mut(repr, other, self.modulus.borrow())
    }

    #[inline]
    fn repr_partial_left_div_mut(
        &self,
        repr: &mut Self::MutRepr,
        other: &Self::Repr,
    ) -> Result<(), ()> {
        self.base_ring
            .repr_partial_div_mod_mut(repr, other, self.modulus.borrow())
    }

    #[inline]
    fn repr_is_unit(&self, repr: &Self::Repr) -> bool {
        self.base_ring.repr_is_unit_mod(repr, self.modulus.borrow())
    }
}

impl<T, R> DivisionRing for Quo<T, R>
where
    T: QuoRem,
    R: Borrow<T::Repr>,
{
}
