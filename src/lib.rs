//! A crate for abstract algebra in particular rings and polynomials.
//!
//! # High-level design
//!
//! This crate's design is founded on a few constraints. First, while rings
//! defined at compile time have the greatest potential in terms of run-time
//! performance, needing a separate type for each algebraic structure comes at a
//! cost in terms of flexibility. Because of this, this library is designed
//! around algebraic structures being run-time values.
//!
//! Since each instance of a type thus represents a concrete algebraic
//! structure, it follows that the type itself represents a concrete family of
//! algebraic structures.
//!
//! In addition to this constraint, this library is shaped by considering two
//! design goals:
//!
//! * The library should be as safe to use as practical.
//! * The library should get as little in the way of performance as possible.
//!
//! To facilitate the first design goal, each element is composed of both a
//! reference to the algebraic structure it belongs to (the “parent” of the
//! element) and the raw representation of the element. When an operation on two
//! or more elements is performed, the parents are checked for equality to
//! prevent accidentally reinterpreting elements from one structure as elements
//! of another. This is necessary to do at run-time because the type system
//! cannot distinguish between concrete rings, say, if they have the same type
//! (i.e., belong to the same family of rings with the same implementation).
//!
//! This, however, leads to a problem with the second design goal. If every
//! element needs to know its parent, then that can quickly lead to a lot of
//! superfluous pointers in structures with elements composed of other elements
//! such as polynomials, matrices, and modules in general. Additionally, it
//! becomes expensive to convert an element from one structure to an element of
//! another structure even if they use the same representation.
//!
//! To solve this issue, references to elements are represented as two
//! references to potentially different areas of memory: one for the
//! representation and one for the parent. This is done in the [`ElemRef`] and
//! [`ElemMut`] types, and in the [`Element`] and [`ElementMut`] traits. As a
//! consequence of this, each of these types and traits take two lifetimes: the
//! lifetime of the raw representation and the lifetime of the parent. As a
//! convention, we denote the lifetimes of representations by `'a`, `'b`, `'c`,
//! etc., while we denote the lifetimes of parents by `'p`, `'q`, `'r`, etc.
//!
//! The type [`Elem`] consists of a reference to the parent and an owned
//! representation. It thus only has one lifetime `'p`.
//!
//! It is possible that elements that own their parents may prove useful, but
//! they are more complicated to work with and have not yet been necessary. They
//! are mostly compatible with the existing system, however.
//!
//! # Safe interface
//!
//! ## The concrete element types
//!
//! The safe interface acts on elements. There are three inherent element types,
//! [`Elem`], [`ElemRef`], and [`ElemMut`]. The first, `Elem`, represents an
//! owned element of an algebraic structure, that is an element whose
//! representation is owned. The other two types, `ElemRef` and `ElemMut`,
//! instead represent borrowed and mutably borrowed elements respectively, that
//! is elements whose representation is borrowed or mutably borrowed.
//!
//! These types form a hierarchy, according to the following conversion rules:
//!
//! * `&Elem` → `ElemRef` using [`Element::as_ref`].
//! * `&mut Elem` → `ElemMut` using [`ElementMut::as_mut`].
//! * `ElemMut` → `ElemRef` using [`Element::as_ref`].
//!
//! Hence, `Elem` is at the top and `ElemRef` is at the bottom. All of these
//! types can also be “upgraded” to an `Elem`, but this involves cloning the
//! representation. This is done using [`Element::to_elem`].
//!
//! ## The element traits
//!
//! Most operations on elements can be found on the [`Element`] and
//! [`ElementMut`] traits. To use these operations, these traits must thus be
//! imported and in scope. Since functions on elements may need to reason about
//! both lifetimes of the element, they are only implemented on references,
//! however.
//!
//! This means `Element` is directly implemented for `ElemRef` and `ElemMut`,
//! while `ElementMut` is only directly implemented for `ElemMut`. They are,
//! however, also implemented for all references to types that can be “borrowed
//! as elements” as defined by the [`BorrowElem`] and [`BorrowElemMut`] traits.
//! The trait `BorrowElem` is implemented for `ElemRef`, `ElemMut`, and `Elem`,
//! as well as transitively for references, while `BorrowElemMut` is implemented
//! for `ElemMut`, `Elem`, and transitively for mutable references.
//!
//! Note that when accessing an element through `BorrowElem` or `BorrowElemMut`
//! the lifetime of the representation is fixed to that of the element itself.
//! If a longer lifetime is needed, using `Element` or `ElementMut` directly, or
//! using one of the concrete element reference types is necessary.
//!
//! As a general rule, when an operation takes multiple elements from the same
//! structure, the parent lifetime is copied from the receiver (`self`) or, if
//! there is no receiver or it has an incompatible parent, the left-most
//! operand. For example, the result of `a.add(&b)` has the parent reference
//! from `a` and not `b`. To override this, you can use the functions
//! [`Elem::with_parent`], [`ElemRef::with_parent`], and
//! [`ElemMut::with_parent`].
//!
//! To add additional operations to elements of certain algebraic structures,
//! extension traits of `Element` and `ElementMut` can be used. The traits
//! [`VecPolynomialsElement`], [`SquareMatsElement`], and
//! [`SquareMatsElementMut`] are examples of this pattern.
//!
//! For an example of how to write a function that takes and returns elements,
//! see [Make a function that takes and returns elements].
//!
//! # Raw interface
//!
//! When defining operations on an algebraic structure, the raw interface is
//! used. This interface is accessed by calling functions directly on the parent
//! and passing in raw representations. Functions that act on raw
//! representations are prefixed with `repr_` where functions that act on
//! elements (i.e., on instances of [`Elem`], [`ElemRef`] or [`ElemMut`]) have
//! no prefix.
//!
//! It is usually not necessary to use the raw interface except when definining
//! basic operations on algebraic structures or when extra control over
//! performance is sought. The raw interface can primarily improve perfomance by
//! skipping equality checks for parents. As the safe interface is a thin
//! wrapper around the raw interface, and since many parent equality checks are
//! cheap or free, it is unlikely to significantly improve performance.
//!
//! For an example of how to define a ring, which uses the raw interface, see
//! [How to make a ring].
//!
//! # Tutorials
//!
//! * [How to make a ring].
//! * [How to use existing rings].
//! * [Make a function that takes and returns elements].
//!
//! [How to make a ring]: docs::make_a_ring
//! [How to use existing rings]: docs::use_a_ring
//! [Make a function that takes and returns elements]: docs::make_a_function

use std::fmt::{self, Debug};
use std::iter;
use std::marker::PhantomData;

use rand::Rng;

pub use self::elem::*;
pub use self::mat::*;
pub use self::poly::*;
pub use self::quo_rem::*;
pub use self::ring::*;
pub use self::structure::*;

pub mod elem;
pub mod mat;
pub mod poly;
pub mod quo_rem;
pub mod ring;
pub mod structure;

#[cfg(doc)]
pub mod docs;

/*
fn repeat<N, F, G>(count: N, mut shift: F, mut update: G)
where
    N: Natural,
    F: FnMut(),
    G: FnMut(),
{
    for k in (0..count.bit_len()).rev() {
        shift();

        if count.bit(k) {
            update();
        }
    }
}
*/

fn repeat_mut<N, T, F, G>(count: N, state: &mut T, mut shift: F, mut update: G)
where
    N: Natural,
    T: ?Sized,
    F: FnMut(&mut T),
    G: FnMut(&mut T),
{
    for bit in count.bits().rev() {
        shift(state);

        if bit {
            update(state);
        }
    }
}

/// A trait for uniformly sampling elements of a finite algebraic structure.
pub trait RandomElem: Structure {
    /// Returns a random representation using `rng`.
    fn repr_random_elem<R>(&self, rng: R) -> Self::OwnedRepr
    where
        R: Rng;

    /// Returns a random element using `rng`.
    fn random_elem<R>(&self, rng: R) -> Elem<Self>
    where
        R: Rng,
    {
        let repr = self.repr_random_elem(rng);
        self.wrap_unchecked(repr)
    }
}

/// A trait for uniformly sampling remainders modulo something in an algebraic
/// structure.
pub trait RandomElemMod: Structure {
    /// Returns a random representation of a remainder modulo `modulus` using
    /// `rng`.
    fn repr_random_elem_mod<R>(&self, rng: R, modulus: &Self::Repr) -> Self::OwnedRepr
    where
        R: Rng;

    /// Returns a random remainder modulo `modulus` using `rng`.
    fn random_elem_mod<'q, R, M>(&self, rng: R, modulus: M) -> Elem<Self>
    where
        Self: 'q,
        R: Rng,
        M: BorrowElem<'q, Self>,
    {
        let (repr, parent) = modulus.pair();

        assert!(self == parent);

        let repr = self.repr_random_elem_mod(rng, repr);
        self.wrap_unchecked(repr)
    }
}

impl<T> RandomElem for Quo<T>
where
    T: QuoRem + RandomElemMod,
{
    fn repr_random_elem<R>(&self, rng: R) -> Self::OwnedRepr
    where
        R: Rng,
    {
        self.base_ring()
            .repr_random_elem_mod(rng, self.repr_modulus())
    }
}

impl RandomElemMod for Ints<i32> {
    fn repr_random_elem_mod<R>(&self, mut rng: R, modulus: &Self::Repr) -> Self::OwnedRepr
    where
        R: Rng,
    {
        rng.gen_range(0, *modulus)
    }
}

impl<T> RandomElemMod for VecPolynomials<T>
where
    T: Ring + RandomElem,
{
    fn repr_random_elem_mod<R>(&self, mut rng: R, modulus: &Self::Repr) -> Self::OwnedRepr
    where
        R: Rng,
    {
        let mut result = iter::repeat_with(|| self.base_ring().repr_random_elem(&mut rng))
            .take(modulus.len() - 1)
            .collect();
        self.repr_truncate(&mut result);
        result
    }
}

impl RandomElem for ModP<i32> {
    fn repr_random_elem<R>(&self, mut rng: R) -> Self::OwnedRepr
    where
        R: Rng,
    {
        rng.gen_range(0, self.prime)
    }
}

impl<T> RandomElem for SquareMats<T>
where
    T: RandomElem,
{
    fn repr_random_elem<R>(&self, mut rng: R) -> Self::OwnedRepr
    where
        R: Rng,
    {
        self.repr_with(|_, _| self.base_ring().repr_random_elem(&mut rng))
    }
}

/// Integer ring with representation `T`.
pub struct Ints<T>(PhantomData<T>);

impl<T> Ints<T> {
    /// Returns the singleton ring of integers.
    pub const fn new() -> Self {
        Ints(PhantomData)
    }
}

impl<T> Ints<T>
where
    T: Clone,
{
    /// Returns an element with value `value`.
    pub fn wrap(&self, value: T) -> Elem<Self> {
        self.wrap_unchecked(value)
    }
}

impl<T> PartialEq for Ints<T> {
    #[inline]
    fn eq(&self, _: &Self) -> bool {
        true
    }
}

impl<T> Clone for Ints<T> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<T> Copy for Ints<T> {}

impl<T> Structure for Ints<T>
where
    T: Clone,
{
    type OwnedRepr = T;
    type MutRepr = T;
    type Repr = T;
}

impl ReprEq for Ints<i32> {
    fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
        *repr == *other
    }
}

impl<T> Debug for Ints<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Ints")
    }
}

impl ReprDebug for Ints<i32> {
    fn fmt(&self, repr: &Self::Repr, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", *repr)
    }
}

impl Ring for Ints<i32> {
    #[inline]
    fn repr_zero(&self) -> Self::OwnedRepr {
        0
    }

    #[inline]
    fn repr_one(&self) -> Self::OwnedRepr {
        1
    }

    #[inline]
    fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        *repr + *other
    }

    #[inline]
    fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        *repr - *other
    }

    #[inline]
    fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        *repr * *other
    }

    #[inline]
    fn is_trivial(&self) -> bool {
        false
    }
}

impl CommutRing for Ints<i32> {}

impl QuoRem for Ints<i32> {
    #[inline]
    fn repr_quo_rem(
        &self,
        repr: &Self::Repr,
        other: &Self::Repr,
    ) -> (Self::OwnedRepr, Self::OwnedRepr) {
        (self.repr_quo(repr, other), self.repr_rem(repr, other))
    }

    #[inline]
    fn repr_quo(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        repr.div_euclid(*other)
    }

    #[inline]
    fn repr_rem(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        repr.rem_euclid(*other)
    }
}

#[test]
fn test_ints() {
    let parent = Ints::<i32>::new();

    assert!(parent.zero() == parent.wrap(0));
    assert!(parent.one() == parent.wrap(1));
    assert!(parent.zero() != parent.one());

    assert!(parent.zero().is_zero());
    assert!(!parent.zero().is_one());
    assert!(parent.one().is_one());
    assert!(!parent.one().is_zero());
}

#[test]
fn test_ints_gcd() {
    let parent = Ints::<i32>::new();

    let a = parent.wrap(5040);
    let b = parent.wrap(712);

    assert_eq!(
        a.gcd_ext(&b),
        (parent.wrap(8), parent.wrap(-38), parent.wrap(269))
    );
}

#[test]
fn test_ints_mul_nat() {
    let parent = Ints::<i32>::new();

    let a = parent.wrap(1234);

    assert_eq!(a.mul_nat(7382), parent.wrap(9109388));
}

fn gcd_ext(a: i32, b: i32) -> (i32, i32, i32) {
    let parent = Ints::new();
    parent.repr_gcd_ext(&a, &b)
}

/// Field of integers modulo a prime with representation `T`.
#[derive(Clone, PartialEq)]
pub struct ModP<T> {
    prime: T,
}

impl<T> ModP<T> {
    /// Returns the integer quotient ring modulo `prime`, which must be a prime.
    pub const fn new_unchecked(prime: T) -> Self {
        ModP { prime }
    }
}

impl ModP<i32> {
    /// Returns the element (coset) corresponding to `value` in `self`.
    pub fn wrap(&self, value: i32) -> Elem<Self> {
        self.wrap_unchecked(value.rem_euclid(self.prime))
    }
}

impl<T> Structure for ModP<T>
where
    T: Clone + PartialEq,
{
    type OwnedRepr = T;
    type MutRepr = T;
    type Repr = T;
}

impl ReprEq for ModP<i32> {
    fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
        *repr == *other
    }
}

impl<T> Debug for ModP<T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_tuple("ModP").field(&self.prime).finish()
    }
}

impl ReprDebug for ModP<i32> {
    fn fmt(&self, repr: &Self::Repr, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", repr)
    }
}

impl Ring for ModP<i32> {
    #[inline]
    fn repr_zero(&self) -> Self::OwnedRepr {
        0
    }

    #[inline]
    fn repr_one(&self) -> Self::OwnedRepr {
        1_i32.rem_euclid(self.prime)
    }

    #[inline]
    fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        (*repr + *other).rem_euclid(self.prime)
    }

    #[inline]
    fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        (*repr - *other).rem_euclid(self.prime)
    }

    #[inline]
    fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        (*repr * *other).rem_euclid(self.prime)
    }

    #[inline]
    fn is_trivial(&self) -> bool {
        self.prime == 1
    }
}

impl CommutRing for ModP<i32> {}

impl ReprPartialDiv for ModP<i32> {
    #[inline]
    fn repr_partial_div(&self, repr: &Self::Repr, other: &Self::Repr) -> Option<Self::OwnedRepr> {
        Some((*repr * self.repr_inv(other)).rem_euclid(self.prime))
    }

    #[inline]
    fn repr_partial_left_div(
        &self,
        repr: &Self::Repr,
        other: &Self::Repr,
    ) -> Option<Self::OwnedRepr> {
        self.repr_partial_div(repr, other)
    }

    #[inline]
    fn repr_partial_inv(&self, repr: &Self::Repr) -> Option<Self::OwnedRepr> {
        let (r, s, _) = gcd_ext(*repr, self.prime);
        assert!(r == 1);
        Some(s.rem_euclid(self.prime))
    }
}

impl DivisionRing for ModP<i32> {}

fn _assert_elem_ref<'a, 'p, T, E>(mut x: E)
where
    T: Structure + 'p,
    T::Repr: 'a,
    T::MutRepr: 'a,
    E: ElementMut<'a, 'p, T>,
{
    fn g<'b, 'q, T, E>(_x: E)
    where
        T: Structure + 'q,
        T::Repr: 'b,
        E: Element<'b, 'q, T>,
    {
    }

    fn h<'b, 'q, T, E>(_x: E)
    where
        T: Structure + 'q,
        T::Repr: 'b,
        T::MutRepr: 'b,
        E: ElementMut<'b, 'q, T>,
    {
    }

    g(&x);
    h(&mut x);
    h(x.by_ref());
}
