use polymath::{Element, Ints, ModP, Quo, Ring, Structure, VecPolynomials, VecPolynomialsElement};
use criterion::{criterion_group, criterion_main, Criterion};

fn poly_i32(c: &mut Criterion) {
    let parent = VecPolynomials::new(Ints::<i32>::new());
    let p = parent.from_coeff_reprs([45, 45, 3, 5645, 646, 7, 34].iter().cloned());

    c.bench_function("Polynomial addition in Z", |b| b.iter(|| p.add(&p)));
    c.bench_function("Polynomial multiplication in Z", |b| b.iter(|| p.mul(&p)));
}

fn poly_modp(c: &mut Criterion) {
    let parent = VecPolynomials::new(ModP::new_unchecked(13));
    let p = parent.from_coeff_reprs([6, 6, 3, 3, 9, 7, 8].iter().cloned());

    c.bench_function("Polynomial addition in F13", |b| b.iter(|| p.add(&p)));
    c.bench_function("Polynomial multiplication in F13", |b| b.iter(|| p.mul(&p)));
}

fn poly_quo(c: &mut Criterion) {
    let parent = VecPolynomials::new(Quo::new_unchecked(Ints::new(), 13));
    let p = parent.from_coeff_reprs([6, 6, 3, 3, 9, 7, 8].iter().cloned());

    c.bench_function("Polynomial addition in F13 (using Quo)", |b| {
        b.iter(|| p.add(&p))
    });
    c.bench_function("Polynomial multiplication in F13 (using Quo)", |b| {
        b.iter(|| p.mul(&p))
    });
}

fn poly_eval(c: &mut Criterion) {
    let mut g = c.benchmark_group("Polynomial evaluation");
    let f13 = ModP::new_unchecked(13);
    let f13x = VecPolynomials::new(f13.clone());
    let p = f13x.from_coeff_reprs([6, 6, 3, 3, 9, 7, 8].iter().cloned());
    let x = f13.wrap_unchecked(7);

    g.bench_function("Eval in user code", |b| {
        b.iter(|| {
            let mut r = x.parent().zero();
            for c in p.coeffs().rev() {
                r = r.mul(&x).add(&c);
            }
            r
        })
    });

    g.bench_function("Inline eval method", |b| {
        b.iter(|| {
            let (x, par) = x.pair();
            let (coeffs, parent) = p.pair();
            assert!(par == parent.base_ring());

            let mut r = par.repr_zero();
            for c in coeffs.iter().rev() {
                r = par.repr_mul(&r, &x);
                r = par.repr_add(&r, &c);
            }
            parent.base_ring().wrap_unchecked(r)
        })
    });

    g.bench_function("eval method", |b| b.iter(|| p.eval(&x)));
}

criterion_group!(benches, poly_i32, poly_modp, poly_quo, poly_eval);
criterion_main!(benches);
