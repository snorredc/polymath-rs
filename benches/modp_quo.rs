use polymath::{Element, Ints, ModP, Quo};
use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn modp_quo(c: &mut Criterion) {
    let mut g = c.benchmark_group("f13 mul");

    let f13_modp = ModP::new_unchecked(13);
    let f13_quo = Quo::from(Ints::new().wrap(13));

    let a_modp = black_box(f13_modp.wrap(11));
    let b_modp = black_box(f13_modp.wrap(7));

    let a_quo = black_box(f13_quo.wrap(f13_quo.base_ring().wrap(11)));
    let b_quo = black_box(f13_quo.wrap(f13_quo.base_ring().wrap(7)));

    g.bench_function("modp", |b| b.iter(|| a_modp.mul(&b_modp)));
    g.bench_function("quo", |b| b.iter(|| a_quo.mul(&b_quo)));
}

criterion_group!(benches, modp_quo);
criterion_main!(benches);
