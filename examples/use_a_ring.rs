use polymath::{Element, ModP, VecPolynomials};

fn main() {
    let f13x = VecPolynomials::new(ModP::new_unchecked(13));
    let f13 = f13x.base_ring();
    let p = f13x.from_coeff_reprs(vec![1, 2, 3, 4]);
    eprintln!("p = {:?}", p);
    // assert_eq!(p.repr(), &[1, 2, 3, 4]);

    let coeffs = vec![1, -2, 3, -4].into_iter().map(|x| f13.wrap(x));
    let q = f13x.from_coeffs(coeffs);
    eprintln!("q = {:?}", q);
    // assert_eq!(q.repr(), &[1, 11, 3, 9]);

    let r = p.add(&q);
    println!("p + q = {:?}", r);
    // assert_eq!(r, f13x.from_coeff_reprs(vec![2, 0, 6]));
}
