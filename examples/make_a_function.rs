use polymath::{BorrowElem, BorrowElemMut, ElementMut, ModP, Ring, VecPolynomials};

fn add_twice_mut<'p, 'q, T, A, B>(mut a: A, b: B)
where
    T: Ring + 'p + 'q,
    A: BorrowElemMut<'p, T>,
    B: BorrowElem<'q, T>,
{
    a.add_mut(&b);
    a.add_mut(&b);
}

fn main() {
    let f29 = ModP::new_unchecked(29);

    let mut a = f29.wrap(15);
    let b = f29.wrap(-10);
    eprintln!("a = {:?}", a);
    eprintln!("b = {:?}", b);

    add_twice_mut(&mut a, b);
    println!("a + 2b = {:?}", a);
    println!();

    let f29x = VecPolynomials::new(f29);
    let f29 = f29x.base_ring();

    let mut p = f29x.from_coeffs(vec![1, -2, 3, -4].iter().map(|x| f29.wrap(*x)));
    let q = f29x.from_coeffs(vec![0, 0, 1, 1].iter().map(|x| f29.wrap(*x)));
    eprintln!("p = {:?}", p);
    eprintln!("q = {:?}", q);

    add_twice_mut(&mut p, q);
    println!("p + 2q = {:?}", p);
}
