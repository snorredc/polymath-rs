use polymath::{CommutRing, Element, ReprDebug, ReprEq, Ring, Structure};
use std::fmt::{self, Debug, Formatter};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Pairs;

impl Structure for Pairs {
    type OwnedRepr = (i32, i32);
    type MutRepr = (i32, i32);
    type Repr = (i32, i32);
}

impl ReprEq for Pairs {
    fn repr_eq(&self, repr: &Self::Repr, other: &Self::Repr) -> bool {
        repr == other
    }
}

impl Ring for Pairs {
    fn repr_zero(&self) -> Self::OwnedRepr {
        (0, 0)
    }

    fn repr_one(&self) -> Self::OwnedRepr {
        (1, 1)
    }

    fn repr_add(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        (repr.0 + other.0, repr.1 + other.1)
    }

    fn repr_sub(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        (repr.0 - other.0, repr.1 - other.1)
    }

    fn repr_mul(&self, repr: &Self::Repr, other: &Self::Repr) -> Self::OwnedRepr {
        (repr.0 * other.0, repr.1 * other.1)
    }
}

impl CommutRing for Pairs {}

impl ReprDebug for Pairs {
    fn fmt(&self, repr: &Self::Repr, f: &mut Formatter) -> fmt::Result {
        repr.fmt(f)
    }
}

fn main() {
    let parent = Pairs;

    let a = parent.one();
    let b = parent.wrap_unchecked((12, 7));

    eprintln!("a =     {:2?}", a);
    eprintln!("b =     {:2?}", b);
    eprintln!("b + a = {:2?}", b.add(&a));
}
